# -*- coding: utf-8 -*-

import random
import os
from tabnanny import verbose


import numpy as np
import tensorflow as tf

from sklearn.model_selection import KFold

from JSLog import JSLog
from JSData import JSData
from JSModels import EEGNet
from JSExperiment import JSExperiment

def JSExperiment_EEGNet():
    int_seed = 1234
    str_seed = '1234'

    os.environ['PYTHONHASHSEED'] = str_seed
    os.environ['TF_DETERMINISTIC_OPS'] = '0'

    np.random.seed(int_seed)
    random.seed(int_seed)
    tf.random.set_seed(int_seed)

    jsExperiment = JSExperiment(
        "EEGNET with automatic clean data with ASR", # Description
        "EEGNet", # Name
        ['KERNL', 'F1', 'F2', 'D', 'DR', 'DT', 'EP','BS'], # Parameters
        'EEGNet' # model
        )

    print("EXPERIMENT NAME: ", jsExperiment.name)
    
    jsLog = JSLog(jsExperiment.name)

    data_type = 'raw'
    jsData = JSData(data_type)

    num_subjects = 60
    index_subjects = np.arange(1,num_subjects+1)

    kf = KFold(n_splits=10)

    jsExperiment.start_experiment()

    jsLog.init_results(jsExperiment.parameters)
    fold_iter = 1
    for train, test in kf.split(index_subjects):
        
        train = train + 1
        test = test +1
        print("%s %s" % (train, test))

        partial_x_train, partial_y_train, x_val, y_val, x_test, y_test = jsData.JSGetTrainTest_Categorical( train, test, 5, int_seed)

        print(partial_x_train.shape)
        print(partial_y_train.shape)

        print(x_val.shape)
        print(y_val.shape)

        print(x_test.shape)
        print(y_test.shape)

        print(partial_x_train[0])

        # Shape adaptation for EEGNet
        partial_x_train = np.expand_dims(partial_x_train,3)
        x_val = np.expand_dims(x_val,3)
        x_test = np.expand_dims(x_test,3)

        print(partial_x_train[0])

        print("Shape dims: ")
        print(partial_x_train.shape)
        
        for kern_length in [128, 64, 32]:
            for f1 in [128, 64, 32, 16, 8]:
                for f2 in [128, 64, 32, 16, 8]:
                    
                    d = 2 
                    dropout_rate_block = 0.5
                    experiment_epochs = 15
                    experiment_batch_size = 128

                    experiment_parameters_values = [kern_length, f1, f2, d, 
                        dropout_rate_block, data_type, 
                        experiment_epochs, experiment_batch_size]

                    model = EEGNet(2, Chans = partial_x_train.shape[1], Samples = partial_x_train.shape[2], 
                        dropoutRate = dropout_rate_block, kernLength = kern_length, F1 = f1, 
                        D = d, F2 = f2, norm_rate = 0.25, dropoutType = 'Dropout')

                    model.compile(
                        optimizer='adam',
                        loss='binary_crossentropy',
                        metrics=[tf.keras.metrics.BinaryAccuracy(),
                                tf.keras.metrics.Precision(),
                                tf.keras.metrics.Recall(),
                                tf.keras.metrics.TruePositives(),
                                tf.keras.metrics.TrueNegatives(),
                                tf.keras.metrics.FalsePositives(),
                                tf.keras.metrics.FalseNegatives()])

                    
                    print(model.summary())
                    print(experiment_parameters_values)

                    history = model.fit(
                        partial_x_train,
                        partial_y_train,
                        epochs=experiment_epochs,
                        batch_size=experiment_batch_size,
                        validation_data=(x_val, y_val),
                        verbose=1)

                    results = model.evaluate(x_test, y_test)

                    jsLog.JSSaveResults(
                        experiment_parameters_values, 
                        fold_iter, 
                        results)

                    jsLog.JSSaveHistory(
                        history, 
                        experiment_parameters_values, 
                        jsExperiment.parameters, 
                        jsExperiment.get_history_filename(
                            experiment_parameters_values,
                            fold_iter))
        
                    jsLog.JSEvaluateBySubjectBinary(
                        model,
                        jsExperiment.parameters,
                        experiment_parameters_values,
                        test, 
                        jsData, 
                        jsExperiment.get_predictions_filename(
                            experiment_parameters_values,
                            fold_iter))

        fold_iter = fold_iter + 1