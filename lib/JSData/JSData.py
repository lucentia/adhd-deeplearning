import json

import numpy as np
import pandas as pd

from os import path
from numpy import isin
from numpy import random
from numpy import delete
from numpy import where
from sklearn.utils import shuffle

import JSPreprocessing

class JSData:

    def __init__(self, data_name):

        self.dir_path = path.dirname(path.realpath(__file__))
        self.dir_data = self.getPath()
        
        print(self.dir_data)

        print("Loading Data Format: ", data_name)
        self.num_channels = -1
        self.length_channels = -1

        self.num_subjects = 60
        self.sample_frequency = 128  
        self.sample_rate = 128 

        self.load_raw_data()

    def getPath(self):
        configuration_file = path.join(path.dirname(path.realpath(__file__)), '..', '..', 'configuration.json')
        dir_data = ""
        with open(configuration_file) as f:
            config_data = json.load(f)

            print(config_data)

            if(config_data['base_data_path'] and path.isdir(config_data['base_data_path'])):
                dir_data = config_data['base_data_path']
            else:
                default_data_path = path.join(self.dir_path, '..', '..', 'data')
                if(path.isdir(default_data_path)):
                    dir_data = default_data_path

            return dir_data

            


            
    def load_raw_data(self):
        dir_path_control = path.join(self.dir_data, 'ds', 'CONTROL', 'csv')
        dir_path_adhd = path.join(self.dir_data, 'ds', 'ADHD', 'csv')

        sampling_frequency = 128
        window_size = int(2*sampling_frequency)
        overlapping = int(0.5*sampling_frequency)   

        subject_list = []
        sample_list = []
        label_list = []
        self.num_subjects = 60
        self.sample_frequency = sampling_frequency  
        self.sample_rate = sampling_frequency 
        for subject in range(1,61):
            subject_code = "{}".format(subject).zfill(2)
            
            subject_file_control = path.join(dir_path_control, subject_code + ".csv")
            data_control = pd.read_csv(subject_file_control, sep="\t")
            np_array_control = data_control.drop(columns=['Time']).to_numpy()
            start = 0
            np_array_control = JSPreprocessing.JSScaleEEGSample(np_array_control)

            
            
            np_array_control = np_array_control.transpose()

            print("Subject Control: {} [{}]".format(subject, np_array_control.shape))
            while start + window_size < np_array_control.shape[1]:
                #print("Start: {} End:{}".format(start, start+window_size))
                sample = np_array_control[:,start:start + window_size]
                sample_list.append(sample)
                label_list.append((subject,0))

                start += overlapping

            subject_file_adhd = path.join(dir_path_adhd, subject_code + ".csv")
            data_adhd = pd.read_csv(subject_file_adhd, sep="\t")

            np_array_adhd = data_adhd.drop(columns=['Time']).to_numpy()

            np_array_adhd = JSPreprocessing.JSScaleEEGSample(np_array_adhd)
            np_array_adhd = np_array_adhd.transpose()

            start = 0
            
            print("Subject Adhd: {} [{}]".format(subject, np_array_adhd.shape))
            while start + window_size < np_array_adhd.shape[1]:
                #print("Start: {} End:{}".format(start, start+window_size))
                sample = np_array_adhd[:,start:start + window_size]
                sample_list.append(sample)
                label_list.append((subject,1))

                start += overlapping
            
            #print(len(sample_list))
            #print(len(label_list))
            
        self.np_data = np.array(sample_list)
        self.np_labels = np.array(label_list)

        print(self.np_data.shape)
        print(self.np_labels.shape)


    


    def JSGetSingleSubjectBinary(self, id_subject, type_subject):
        mask_subject = isin(self.np_labels[:,0], id_subject) & isin(self.np_labels[:,1], type_subject)
        return mask_subject, self.np_data[mask_subject]

    def JSGetSingleSubjectRaw(self, id_subject, type_subject):
        mask_subject = isin(self.np_labels[:,0], id_subject) & isin(self.np_labels[:,type_subject], [1])
        return self.np_data[mask_subject]

    def JSGetSingleSubject(self, id_subject, type_subject):
        mask_subject = isin(self.np_labels[:,0], id_subject) & isin(self.np_labels[:,type_subject], [1])
        ret_data = self.np_data[mask_subject]
        return ret_data.reshape(ret_data.shape[0], self.num_channels * self.length_channels)

    def JSGetTrainTest_Categorical(self, train_subjects, test_subjects, num_validation_subjects, int_seed):

        x_val_subjects = random.choice(train_subjects, num_validation_subjects, False)
        partial_x_train_subjects = train_subjects
        
        for val_subject in x_val_subjects:
            partial_x_train_subjects = delete(partial_x_train_subjects, where(partial_x_train_subjects==val_subject))

        partial_x_train_mask = isin(self.np_labels[:,0], partial_x_train_subjects)
        x_val_mask = isin(self.np_labels[:,0], x_val_subjects)

        partial_x_train = self.np_data[partial_x_train_mask]
        x_val = self.np_data[x_val_mask]

        partial_y_train = self.np_labels[partial_x_train_mask, 1:]
        y_val = self.np_labels[x_val_mask, 1:]

        x_test_mask = isin(self.np_labels[:,0], test_subjects)

        x_test = self.np_data[x_test_mask]
        y_test = self.np_labels[x_test_mask, 1:]

        partial_x_train, partial_y_train = shuffle(
            partial_x_train, 
            partial_y_train,
            random_state=int_seed)

        x_val, y_val = shuffle(
            x_val, 
            y_val,
            random_state=int_seed)
        
        x_test, y_test = shuffle(
            x_test, 
            y_test,
            random_state=int_seed)

        return partial_x_train, partial_y_train, x_val, y_val, x_test, y_test
