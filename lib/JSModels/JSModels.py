from tensorflow.keras import Input
from tensorflow.keras import Model

from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout

from tensorflow.keras.layers import Activation

from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D

from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import DepthwiseConv2D
from tensorflow.keras.layers import AveragePooling2D

from tensorflow.keras.layers import SpatialDropout2D
from tensorflow.keras.layers import SeparableConv2D

from tensorflow.keras.layers import Concatenate

from tensorflow.keras.layers import Permute
from tensorflow.keras.layers import SeparableConv1D
from tensorflow.keras.layers import AveragePooling1D
from tensorflow.keras.layers import Average


from tensorflow.keras.constraints import max_norm



def JSModel_MLP_03(
    chans=19, 
    samples=250, 
    U1 = 4, 
    U2 = 16, 
    U3 = 4, 
    use_bias_in = False, 
    dropout_block = 0.5, 
    nb_classes=3, 
    dense_activation='relu'):
    
    input_main  = Input((chans,samples))

    flatten     = Flatten()(input_main)

    dense       = Dense(U1, activation=dense_activation, use_bias = use_bias_in)(flatten)
    dense       = Dropout(dropout_block)(dense)

    dense       = Dense(U2, activation=dense_activation, use_bias = use_bias_in)(dense)
    dense       = Dropout(dropout_block)(dense)

    dense       = Dense(U3, activation=dense_activation, use_bias = use_bias_in)(dense)
    dense       = Dropout(dropout_block)(dense)

    if nb_classes == 2:
        dense     = Dense(1, name = 'outputdense_bin')(dense)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'outputdense_multiclass')(dense)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)

    return Model(inputs=input_main, outputs=modeloutput)

def JSModel_VGGNet(nb_classes, height, width, depth):
    input_main   = Input((height, width, depth))

    block1       = Conv2D(filters=64, kernel_size=3, padding='same', activation='relu')(input_main)
    block1       = Conv2D(filters=64, kernel_size=3, padding='same', activation='relu')(block1)
    block1       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block1)

    block2       = Conv2D(filters=128, kernel_size=3, padding='same', activation='relu')(block1)
    block2       = Conv2D(filters=128, kernel_size=3, padding='same', activation='relu')(block2)
    block2       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block2)

    block3       = Conv2D(filters=256, kernel_size=3, padding='same', activation='relu')(block2)
    block3       = Conv2D(filters=256, kernel_size=3, padding='same', activation='relu')(block3)
    block3       = Conv2D(filters=256, kernel_size=3, padding='same', activation='relu')(block3)
    block3       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block3)

    block4       = Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')(block3)
    block4       = Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')(block4)
    block4       = Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')(block4)
    block4       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block4)

    block5       = Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')(block4)
    block5       = Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')(block5)
    block5       = Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')(block5)
    block5       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block5)

    #Dense Layers
    block6       = Flatten()(block5)
    block6       = Dense(units=4096, activation='relu')(block6)
    block6       = Dropout(0.5)(block6)
    block6       = Dense(units=4096, activation='relu')(block6)
    block6       = Dropout(0.5)(block6)

    if nb_classes == 2:
        dense_out    = Dense(units=1, name = 'outputdense_bin')(block6)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense_out)

    else:
        dense_out    = Dense(units=nb_classes, name = 'outputdense_multiclass')(block6)
        modeloutput  = Activation('softmax', name = 'softmax')(dense_out)

    return Model(inputs=input_main, outputs=modeloutput)

def EEGNet(nb_classes, Chans = 64, Samples = 128, 
             dropoutRate = 0.5, kernLength = 64, F1 = 8, 
             D = 2, F2 = 16, norm_rate = 0.25, dropoutType = 'Dropout'):
    """ Keras Implementation of EEGNet
    http://iopscience.iop.org/article/10.1088/1741-2552/aace8c/meta

    Note that this implements the newest version of EEGNet and NOT the earlier
    version (version v1 and v2 on arxiv). We strongly recommend using this
    architecture as it performs much better and has nicer properties than
    our earlier version. For example:
        
        1. Depthwise Convolutions to learn spatial filters within a 
        temporal convolution. The use of the depth_multiplier option maps 
        exactly to the number of spatial filters learned within a temporal
        filter. This matches the setup of algorithms like FBCSP which learn 
        spatial filters within each filter in a filter-bank. This also limits 
        the number of free parameters to fit when compared to a fully-connected
        convolution. 
        
        2. Separable Convolutions to learn how to optimally combine spatial
        filters across temporal bands. Separable Convolutions are Depthwise
        Convolutions followed by (1x1) Pointwise Convolutions. 
        
    
    While the original paper used Dropout, we found that SpatialDropout2D 
    sometimes produced slightly better results for classification of ERP 
    signals. However, SpatialDropout2D significantly reduced performance 
    on the Oscillatory dataset (SMR, BCI-IV Dataset 2A). We recommend using
    the default Dropout in most cases.
        
    Assumes the input signal is sampled at 128Hz. If you want to use this model
    for any other sampling rate you will need to modify the lengths of temporal
    kernels and average pooling size in blocks 1 and 2 as needed (double the 
    kernel lengths for double the sampling rate, etc). Note that we haven't 
    tested the model performance with this rule so this may not work well. 
    
    The model with default parameters gives the EEGNet-8,2 model as discussed
    in the paper. This model should do pretty well in general, although it is
	advised to do some model searching to get optimal performance on your
	particular dataset.

    We set F2 = F1 * D (number of input filters = number of output filters) for
    the SeparableConv2D layer. We haven't extensively tested other values of this
    parameter (say, F2 < F1 * D for compressed learning, and F2 > F1 * D for
    overcomplete). We believe the main parameters to focus on are F1 and D. 

    Inputs:
        
      nb_classes      : int, number of classes to classify
      Chans, Samples  : number of channels and time points in the EEG data
      dropoutRate     : dropout fraction
      kernLength      : length of temporal convolution in first layer. We found
                        that setting this to be half the sampling rate worked
                        well in practice. For the SMR dataset in particular
                        since the data was high-passed at 4Hz we used a kernel
                        length of 32.     
      F1, F2          : number of temporal filters (F1) and number of pointwise
                        filters (F2) to learn. Default: F1 = 8, F2 = F1 * D. 
      D               : number of spatial filters to learn within each temporal
                        convolution. Default: D = 2
      dropoutType     : Either SpatialDropout2D or Dropout, passed as a string.

    """
    
    if dropoutType == 'SpatialDropout2D':
        dropoutType = SpatialDropout2D
    elif dropoutType == 'Dropout':
        dropoutType = Dropout
    else:
        raise ValueError('dropoutType must be one of SpatialDropout2D '
                         'or Dropout, passed as a string.')
    
    input1   = Input(shape = (Chans, Samples, 1))

    ##################################################################
    block1       = Conv2D(F1, (1, kernLength), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False)(input1)
    block1       = BatchNormalization()(block1)
    block1       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1)
    block1       = BatchNormalization()(block1)
    block1       = Activation('elu')(block1)
    block1       = AveragePooling2D((1, 4))(block1)
    block1       = dropoutType(dropoutRate)(block1)
    
    block2       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1)
    block2       = BatchNormalization()(block2)
    block2       = Activation('elu')(block2)
    block2       = AveragePooling2D((1, 8))(block2)
    block2       = dropoutType(dropoutRate)(block2)
        
    flatten      = Flatten(name = 'flatten')(block2)
    
    if nb_classes == 2:
        dense     = Dense(1, kernel_constraint = max_norm(norm_rate))(flatten)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'dense', 
                            kernel_constraint = max_norm(norm_rate))(flatten)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)
    
    return Model(inputs=input1, outputs=modeloutput)

def JSModel_MH_EEGNet(nb_classes, Chans = 64, Samples = 128, 
             dropoutRate = 0.5, dropoutRateDense = 0.5, samplingRate = 128, F1 = 8, 
             D = 2, F2 = 16, norm_rate = 0.25, dropoutType = 'Dropout', activation='elu'):
        
    if dropoutType == 'SpatialDropout2D':
        dropoutType = SpatialDropout2D
    elif dropoutType == 'Dropout':
        dropoutType = Dropout
    else:
        raise ValueError('dropoutType must be one of SpatialDropout2D '
                         'or Dropout, passed as a string.')
    
    input1   = Input(shape = (Chans, Samples, 1))

    ##################################################################
    
    #### ULF SamplingRate/2
    block1_ulf       = Conv2D(F1, (1, samplingRate//2), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False, name="CONV_ulf")(input1)
    block1_ulf       = BatchNormalization()(block1_ulf)
    block1_ulf       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1_ulf)
    block1_ulf       = BatchNormalization()(block1_ulf)
    block1_ulf       = Activation(activation)(block1_ulf)
    block1_ulf       = AveragePooling2D((1, 4))(block1_ulf)
    block1_ulf_out   = dropoutType(dropoutRate)(block1_ulf)

    block2_ulf       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1_ulf_out)
    block2_ulf       = BatchNormalization()(block2_ulf)
    block2_ulf       = Activation(activation)(block2_ulf)
    block2_ulf       = AveragePooling2D((1, 8))(block2_ulf)
    block2_ulf       = dropoutType(dropoutRate)(block2_ulf)
        
    flatten_ulf      = Flatten(name = 'flatten_ulf')(block2_ulf)

    #### LF SampligRate/4
    block1_lf       = Conv2D(F1, (1, samplingRate//4), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False, name="CONV_lf")(input1)
    block1_lf       = BatchNormalization()(block1_lf)
    block1_lf       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1_lf)
    block1_lf       = BatchNormalization()(block1_lf)
    block1_lf       = Activation(activation)(block1_lf)
    block1_lf       = AveragePooling2D((1, 4))(block1_lf)
    block1_lf_out   = dropoutType(dropoutRate)(block1_lf)

    block2_lf       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1_lf_out)
    block2_lf       = BatchNormalization()(block2_lf)
    block2_lf       = Activation(activation)(block2_lf)
    block2_lf       = AveragePooling2D((1, 8))(block2_lf)
    block2_lf       = dropoutType(dropoutRate)(block2_lf)
        
    flatten_lf      = Flatten(name = 'flatten_lf')(block2_lf)

    #### LF SampligRate/8
    block1_hf       = Conv2D(F1, (1, samplingRate//8), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False, name="CONV_hf")(input1)
    block1_hf       = BatchNormalization()(block1_hf)
    block1_hf       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1_hf)
    block1_hf       = BatchNormalization()(block1_hf)
    block1_hf       = Activation(activation)(block1_hf)
    block1_hf       = AveragePooling2D((1, 4))(block1_hf)
    block1_hf_out   = dropoutType(dropoutRate)(block1_hf)

    block2_hf       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1_hf_out)
    block2_hf       = BatchNormalization()(block2_hf)
    block2_hf       = Activation(activation)(block2_hf)
    block2_hf       = AveragePooling2D((1, 8))(block2_hf)
    block2_hf       = dropoutType(dropoutRate)(block2_hf)
        
    flatten_hf      = Flatten(name = 'flatten_hf')(block2_hf)

    #### LF SampligRate/16
    block1_uhf       = Conv2D(F1, (1, samplingRate//16), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False, name="CONV_uhf")(input1)
    block1_uhf       = BatchNormalization()(block1_uhf)
    block1_uhf       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1_uhf)
    block1_uhf       = BatchNormalization()(block1_uhf)
    block1_uhf       = Activation(activation)(block1_uhf)
    block1_uhf       = AveragePooling2D((1, 4))(block1_uhf)
    block1_uhf_out   = dropoutType(dropoutRate)(block1_uhf)

    block2_uhf       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1_uhf_out)
    block2_uhf       = BatchNormalization()(block2_uhf)
    block2_uhf       = Activation(activation)(block2_uhf)
    block2_uhf       = AveragePooling2D((1, 8))(block2_uhf)
    block2_uhf       = dropoutType(dropoutRate)(block2_uhf)
        
    flatten_uhf      = Flatten(name = 'flatten_uhf')(block2_uhf)

    ##############
    
    flatten = Concatenate(axis=1)([flatten_ulf, flatten_lf, flatten_hf, flatten_uhf])

    if nb_classes == 1:
        dense     = Dense(1, kernel_constraint = max_norm(norm_rate))(flatten)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'dense', 
                            kernel_constraint = max_norm(norm_rate))(flatten)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)
    
    return Model(inputs=input1, outputs=modeloutput)


def JSModel_MH_DSCNet(Chans = 56, Samples = 385, nb_classes=3, dropout_rate=0.5, filters=16, kernel_size=64, default_activation='relu', partial_output_classes=56, conv_1D_padding='valid'):
    input = Input((Chans,Samples))
    
    input_permute = Permute((2, 1))(input)
    # normalization_input = BatchNormalization(name='Input_Normalization')(input_permute)
    
    # ULF Block
    blockULF = SeparableConv1D(filters, kernel_size, activation=default_activation, padding=conv_1D_padding, name='ULF_Conv_01')(input_permute)
    blockULF = SeparableConv1D(filters, kernel_size, activation=default_activation, padding=conv_1D_padding,name='ULF_Conv_02')(blockULF)
    blockULF = AveragePooling1D(pool_size=(2), name='ULF_Pool')(blockULF)
    #blockULF = LSTM(4, return_sequences=True)(blockULF)
    flatULF = Flatten(name='ULF_Flat')(blockULF)

    softmax_ULF = Dense(partial_output_classes, activation='softmax', name='ULF_Activation')(flatULF)
    # normalization_ULF = BatchNormalization(name='ULF_Normalization')(softmax_ULF)
    dropout_ULF = Dropout(dropout_rate, name='ULF_Dropout')(softmax_ULF)

    # LF Block
    blockLF = SeparableConv1D(filters, kernel_size//2, activation=default_activation, padding=conv_1D_padding,name='LF_Conv_01')(input_permute)
    blockLF = SeparableConv1D(filters, kernel_size//2, activation=default_activation, padding=conv_1D_padding,name='LF_Conv_02')(blockLF)
    blockLF = AveragePooling1D(pool_size=(2), name='LF_Pool')(blockLF)
    #blockLF = LSTM(4, return_sequences=True)(blockLF)
    flatLF = Flatten(name='LF_Flat')(blockLF)

    softmax_LF = Dense(partial_output_classes, activation='softmax', name='LF_Activation')(flatLF)
    # normalization_LF = BatchNormalization(name='LF_Normalization')(softmax_LF)
    dropout_LF = Dropout(dropout_rate, name='LF_Dropout')(softmax_LF)

    # MF Block
    blockMF = SeparableConv1D(filters, kernel_size//4, activation=default_activation, padding=conv_1D_padding,name='MF_Conv_01')(input_permute)
    blockMF = SeparableConv1D(filters, kernel_size//4, activation=default_activation, padding=conv_1D_padding,name='MF_Conv_02')(blockMF)
    blockMF = AveragePooling1D(pool_size=(2), name='MF_Pool')(blockMF)
    #blockMF = LSTM(4, return_sequences=True)(blockMF)
    flatMD = Flatten(name='MF_Flat')(blockMF)

    softmax_MF = Dense(partial_output_classes, activation='softmax', name='MF_Activation')(flatMD)
    # normalization_MF = BatchNormalization(name='MF_Normalization')(softmax_MF)
    dropout_MF = Dropout(dropout_rate, name='MF_Dropout')(softmax_MF)

    # HF Block
    blockHF = SeparableConv1D(filters, kernel_size//8, activation=default_activation, padding=conv_1D_padding,name='HF_Conv_01')(input_permute)
    blockHF = SeparableConv1D(filters, kernel_size//8, activation=default_activation, padding=conv_1D_padding,name='HF_Conv_02')(blockHF)
    blockHF = AveragePooling1D(pool_size=(2), name='HF_Pool')(blockHF)
    #blockHF = LSTM(4, return_sequences=True)(blockHF)
    flatHF = Flatten(name='HF_Flat')(blockHF)

    softmax_HF = Dense(partial_output_classes, activation='softmax', name='HF_Activation')(flatHF)
    # normalization_HF = BatchNormalization(name='HF_Normalization')(softmax_HF)
    dropout_HF = Dropout(dropout_rate, name='HF_Dropout')(softmax_HF)

    # UHF Block
    blockUHF = SeparableConv1D(filters, kernel_size//16, activation=default_activation, padding=conv_1D_padding,name='UHF_Conv_01')(input_permute)
    blockUHF = SeparableConv1D(filters, kernel_size//16, activation=default_activation, padding=conv_1D_padding,name='UHF_Conv_02')(blockUHF)
    blockUHF = AveragePooling1D(pool_size=(2), name='UHF_Pool')(blockUHF)
    #blockUHF = LSTM(4, return_sequences=True)(blockUHF)
    flatUHF = Flatten(name='UHF_Flat')(blockUHF)

    softmax_UHF = Dense(partial_output_classes, activation='softmax', name='UHF_Activation')(flatUHF)
    # normalization_UHF = BatchNormalization(name='UHF_Normalization')(softmax_UHF)
    dropout_UHF = Dropout(dropout_rate, name='UHF_Dropout')(softmax_UHF)

    # UUHF Block
    blockUUHF = SeparableConv1D(filters, kernel_size//32, activation=default_activation, padding=conv_1D_padding,name='UUHF_Conv_01')(input_permute)
    blockUUHF = SeparableConv1D(filters, kernel_size//32, activation=default_activation, padding=conv_1D_padding,name='UUHF_Conv_02')(blockUUHF)
    blockUUHF = AveragePooling1D(pool_size=(2), name='UUHF_Pool')(blockUUHF)
    #blockUUHF = LSTM(4, return_sequences=True)(blockUUHF)
    flatUUHF = Flatten(name='UUHF_Flat')(blockUUHF)

    softmax_UUHF = Dense(partial_output_classes, activation='softmax', name='UUHF_Activation')(flatUUHF)
    # normalization_UUHF = BatchNormalization(name='UUHF_Normalization')(softmax_UUHF)
    dropout_UUHF = Dropout(dropout_rate, name='UUHF_Dropout')(softmax_UUHF)

    concatBlock = Average(name='Concat_Blocks')([dropout_ULF, dropout_LF, dropout_MF, dropout_HF, dropout_UHF, dropout_UUHF])

    # flatconcat = Flatten(name='Flat_Concat')(concatBlock)
    dropout = Dropout(dropout_rate, name='Flat_Dropout')(concatBlock)

    if nb_classes == 2:
        dense     = Dense(1, name = 'outputdense_bin')(dropout)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'outputdense_multiclass')(dropout)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)

    # softmax_final = Dense(nb_classes, activation='softmax', name='Softmax_General_Output')(dropout)

    return Model(inputs=input, outputs=modeloutput)