from sklearn.preprocessing import StandardScaler




def JSScaleEEGSample(data):
  return StandardScaler().fit_transform(data)

def JSStandardScale(partial_x_train, x_val, x_test):
  scaler = StandardScaler()
  scaler.fit(partial_x_train)

  return scaler.transform(partial_x_train), scaler.transform(x_val), scaler.transform(x_test)



def jsNormalizeSamples(x_train, x_val, x_test):
  x_train_min = x_train.min()
  x_train_max = x_train.max()

  print("x_train {} - {}".format(x_train.min(), x_train.max()))
  print("x_test {} - {}".format(x_test.min(), x_test.max()))
  print("x_val {} - {}".format(x_val.min(), x_val.max()))

  x_train_n = (x_train - x_train_min) / (x_train_max - x_train_min)
  x_test_n = (x_test - x_train_min) / (x_train_max - x_train_min)
  x_val_n = (x_val - x_train_min) / (x_train_max - x_train_min)

  print("x_train_n {} - {}".format(x_train_n.min(), x_train_n.max()))
  print("x_test_n {} - {}".format(x_test_n.min(), x_test_n.max()))
  print("x_val_n {} - {}".format(x_val_n.min(), x_val_n.max()))

  return x_train_n, x_val_n, x_test_n

def jsScale(x_train, x_val, x_test):
  x_train_max = x_train.max()

  return x_train/x_train_max, x_val/x_train_max, x_test/x_train_max

