import json

from os import path
from os import mkdir
from csv import writer
from datetime import datetime
from sys import exit



class JSLog:

    def __init__(self, experiment_name):

        self.experiment_name = experiment_name
        
        self.dir_path = path.dirname(path.realpath(__file__))
        self.log_path = self.getPath() 

        now_str = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")
        hash_now_str = str(hash(now_str))

        self.experiment_log_name = "{0}_{1}_{2}/".format(
            self.experiment_name,
            now_str,
            hash_now_str.replace('-','_')
        )


        self.experiment_log_path = path.join(self.log_path, self.experiment_log_name)
        #self.model_log_dir_models = self.experiment_log_path + 'models/'
        self.model_log_dir_history = path.join(self.experiment_log_path, 'history')
        self.model_log_dir_predict = path.join(self.experiment_log_path, 'predictions')
        self.model_log_dir_results = path.join(self.experiment_log_path, 'results')

        mkdir(self.experiment_log_path)
        #mkdir(self.model_log_dir_models)
        mkdir(self.model_log_dir_history)
        mkdir(self.model_log_dir_predict)
        mkdir(self.model_log_dir_results)

        self.path_results_file_name = path.join(self.model_log_dir_results, 'results.csv')

    def getPath(self):
        configuration_file = path.join(path.dirname(path.realpath(__file__)), '..', '..', 'configuration.json')
        dir_data = ""
        with open(configuration_file) as f:
            config_data = json.load(f)

            print(config_data)

            if(config_data['base_log_path'] and path.isdir(config_data['base_log_path'])):
                dir_data = config_data['base_log_path']
            else:
                default_log_path = path.join(self.dir_path, '..', '..', 'log')
                if(path.isdir(default_log_path)):
                    dir_data = default_log_path

            return dir_data

    def init_results(self, experiment_parameters):

        reg_params = experiment_parameters + ['iter', 'loss', 'binary_accuracy', 'precision', 'recall', 'tp', 'tn', 'fp', 'fn']
        
        with open(self.path_results_file_name, 'w') as log_file_handler:
            log_writer = writer(log_file_handler)
            log_writer.writerow(reg_params)


    def JSSaveHistory(self, history, experiment_parameters_values, experiment_parameters, history_file_name):

        with open(path.join(self.model_log_dir_history, history_file_name), 'w') as f: 
            w = writer(f)
            history_header = experiment_parameters + ["loss", "acc", "val_loss", "val_acc"]
            w.writerow(history_header)
            for loss, acc, val_loss, val_acc in zip(history.history['loss'], 
                                                    history.history['binary_accuracy'], 
                                                    history.history['val_loss'],   
                                                    history.history['val_binary_accuracy']):
                reg_data = experiment_parameters_values + [loss, acc, val_loss, val_acc]
                w.writerow(reg_data)

    def JSSaveResults(self, parameters_values, fold_iter, results):

        print(parameters_values)
        reg_results = parameters_values + [fold_iter] + results
        print(reg_results)
        with open(self.path_results_file_name, 'a') as f:
            log_writer = writer(f)
            log_writer.writerow(reg_results)

    def JSSaveModel(self, model, model_file_name):
        model.save(self.model_log_dir_models + model_file_name)

    def JSNpToJson(self, results):
        resJson = '{ "results":['
        for data in results:
            resJson = resJson + ',' + str(data[0])
        resJson += ']}'
        return resJson


    def JSEvaluateBySubjectBinary(
        self, 
        model, 
        experiment_parameters,
        experiment_parameters_values,
        test, 
        jsData, 
        predictions_file_name, 
        type_A = 0, 
        type_B = 1):

        with open(path.join(self.model_log_dir_predict, predictions_file_name), 'w') as f:
            w = writer(f)
            predictions_header = experiment_parameters + ["Subject", "SubjectType", "Zeros", "Ones", "Results"]
            w.writerow(predictions_header)

            for subject in test:
                subject_types = [type_A,type_B]
                for subject_type in subject_types:
                    mask_subject, subject_data = jsData.JSGetSingleSubjectBinary(subject, subject_type)
                    results = model.predict(subject_data)

                    num_zeros = results < 0.5
                    num_ones = results >= 0.5
                    predictions_reg_data = experiment_parameters_values + [subject, subject_type, num_zeros.sum(), num_ones.sum(), self.JSNpToJson(results)]
                    w.writerow(predictions_reg_data)
                    print("SUBJECT {} | TYPE {} | ZEROS {} | ONES {}".format(subject, subject_type, num_zeros.sum(), num_ones.sum()))
                    
                    subject_data = None
                    results = None
                    num_zeros = None
                    num_ones = None


    def JSEvaluateBySubject(
        self, 
        model, 
        experiment_parameters,
        experiment_parameters_values,
        test, 
        jsData, 
        predictions_file_name, 
        type_A, 
        type_B):
    
        with open(self.model_log_dir_predict + predictions_file_name, 'w') as f:
            w = writer(f)
            predictions_header = experiment_parameters + ["Subject", "SubjectType", "Zeros", "Ones", "Results"]
            w.writerow(predictions_header)

            for subject in test:
                subject_types = [type_A,type_B]
                for subject_type in subject_types:

                    subject_data = jsData.JSGetSingleSubject(subject, subject_type)
                    results = model.predict(subject_data)

                    num_zeros = results < 0.5
                    num_ones = results >= 0.5
                    predictions_reg_data = experiment_parameters_values + [subject, subject_type, num_zeros.sum(), num_ones.sum(), self.JSNpToJson(results)]
                    w.writerow(predictions_reg_data)
                    print("SUBJECT {} | TYPE {} | ZEROS {} | ONES {}".format(subject, subject_type, num_zeros.sum(), num_ones.sum()))
                    
                    subject_data = None
                    results = None
                    num_zeros = None
                    num_ones = None