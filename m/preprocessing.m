clear;
%% Add path where your eeglab is located
addpath '';
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

%% Add path where your data is located
datapath = "../data/original/";

%% CONTROLS
group = "CONTROL";

% Specify the folder where the files live.
myFolder = sprintf("%s", datapath, group);
% Check to make sure that folder actually exists.  Warn user if it doesn't.
if ~isfolder(myFolder)
    errorMessage = sprintf("Error: The following folder does not exist:\n%s\nPlease specify a new folder.", myFolder);
    uiwait(warndlg(errorMessage));
    myFolder = uigetdir(); % Ask for a new one.
    if myFolder == 0
         % User clicked Cancel
         return;
    end
end
% Get a list of all files in the folder with the desired file name pattern.
filePattern = fullfile(myFolder, '*.mat'); % Change to whatever pattern you need.
theFiles = dir(filePattern);
sample_rate = 128;

chan_locs_file = convertStringsToChars(sprintf("%s", datapath, 'Standard-10-20-Cap19new.ced'));

for k = 1 : length(theFiles)
    baseFileName = theFiles(k).name;
    fullFileName = sprintf("%s", fullfile(theFiles(k).folder, baseFileName));
    [folder, baseFileNameNoExt, extension] = fileparts(fullFileName);
    
    subject_code = sprintf("%0.2d", k);
    dataset_name = convertStringsToChars(sprintf("C%0.2d_%s", k, baseFileNameNoExt));
    dataset_file = convertStringsToChars(sprintf("%s.set", subject_code));
    dataset_file_out_path = convertStringsToChars(sprintf("%s", datapath, "ds/",group,"/"));
    
    fprintf(1, '\n\nNow reading %s [%s] [%s] [%s] \n[%s]\n\n', fullFileName, baseFileNameNoExt, subject_code, dataset_name, chan_locs_file);
    % Now do whatever you want with this file name,
    % such as reading it in as an image array with imread()
    
    EEG = pop_importdata('dataformat','matlab','nbchan',0,'data',fullFileName,'setname',dataset_name,'srate',sample_rate,'subject',subject_code,'pnts',0,'xmin',0,'group',group,'chanlocs',chan_locs_file);
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 0,'gui','off'); 
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename',dataset_file,'filepath', dataset_file_out_path);
    [ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
    
    EEG = pop_eegfiltnew(EEG, 'locutoff',0.5);
    dataset_name = convertStringsToChars(sprintf("%s__F_0_5", dataset_name));
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'setname',dataset_name,'gui','off');
    
    EEG = pop_eegfiltnew(EEG, 'hicutoff',60);
    dataset_name = convertStringsToChars(sprintf("%s__70", dataset_name));
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'setname',dataset_name,'gui','off');
    
    EEG = pop_eegfiltnew(EEG, 'locutoff',49.9,'hicutoff',50.1,'revfilt',1);
    dataset_name = convertStringsToChars(sprintf("%s__N_50", dataset_name));
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'setname',dataset_name,'gui','off');
    
    
    EEG = pop_clean_rawdata(EEG, 'FlatlineCriterion','off','ChannelCriterion','off','LineNoiseCriterion','off','Highpass','off','BurstCriterion',20,'WindowCriterion',0.25,'BurstRejection','on','Distance','Euclidian','WindowCriterionTolerances',[-Inf 7] );
    EEG = pop_clean_rawdata(EEG, 'FlatlineCriterion','off','ChannelCriterion','off','LineNoiseCriterion','off','Highpass','off','BurstCriterion',20,'WindowCriterion',0.25,'BurstRejection','on','Distance','Euclidian','WindowCriterionTolerances',[-Inf 7] );

    EEG = pop_clean_rawdata(EEG, 'FlatlineCriterion','off','ChannelCriterion','off','LineNoiseCriterion','off','Highpass','off','BurstCriterion',20,'WindowCriterion',0.25,'BurstRejection','off','Distance','Euclidian','WindowCriterionTolerances',[-Inf 7] );

    dataset_name = convertStringsToChars(sprintf("%s__ASR", dataset_name));
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'setname',dataset_name,'gui','off');
    
    dataset_file = convertStringsToChars(sprintf("%s_ASR.set", subject_code));
    EEG = pop_saveset( EEG, 'filename',dataset_file,'filepath', dataset_file_out_path);
    
    EEG = eeg_checkset( EEG );
    dataset_csv_path = convertStringsToChars(sprintf("%scsv/%0.2d.csv", dataset_file_out_path, k));
    fprintf(1, 'SAVING [%s]', dataset_csv_path)
    
    pop_export(EEG,dataset_csv_path,'transpose','on','precision',6);
    
end

%% ADHD
group = "ADHD";

% Specify the folder where the files live.
myFolder = sprintf("%s", datapath, group);
% Check to make sure that folder actually exists.  Warn user if it doesn't.
if ~isfolder(myFolder)
    errorMessage = sprintf("Error: The following folder does not exist:\n%s\nPlease specify a new folder.", myFolder);
    uiwait(warndlg(errorMessage));
    myFolder = uigetdir(); % Ask for a new one.
    if myFolder == 0
         % User clicked Cancel
         return;
    end
end
% Get a list of all files in the folder with the desired file name pattern.
filePattern = fullfile(myFolder, '*.mat'); % Change to whatever pattern you need.
theFiles = dir(filePattern);
sample_rate = 128;

chan_locs_file = convertStringsToChars(sprintf("%s", datapath, 'Standard-10-20-Cap19new.ced'));

for k = 1 : length(theFiles)
    baseFileName = theFiles(k).name;
    fullFileName = sprintf("%s", fullfile(theFiles(k).folder, baseFileName));
    [folder, baseFileNameNoExt, extension] = fileparts(fullFileName);
    
    subject_code = sprintf("%0.2d", k);
    dataset_name = convertStringsToChars(sprintf("C%0.2d_%s", k, baseFileNameNoExt));
    dataset_file = convertStringsToChars(sprintf("%s.set", subject_code));
    dataset_file_out_path = convertStringsToChars(sprintf("%s", datapath, "ds/",group,"/"));
    
    fprintf(1, '\n\nNow reading %s [%s] [%s] [%s] \n[%s]\n\n', fullFileName, baseFileNameNoExt, subject_code, dataset_name, chan_locs_file);
    % Now do whatever you want with this file name,
    % such as reading it in as an image array with imread()
    
    EEG = pop_importdata('dataformat','matlab','nbchan',0,'data',fullFileName,'setname',dataset_name,'srate',sample_rate,'subject',subject_code,'pnts',0,'xmin',0,'group',group,'chanlocs',chan_locs_file);
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 0,'gui','off'); 
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename',dataset_file,'filepath', dataset_file_out_path);
    [ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
    
    EEG = pop_eegfiltnew(EEG, 'locutoff',0.5);
    dataset_name = convertStringsToChars(sprintf("%s__F_0_5", dataset_name));
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'setname',dataset_name,'gui','off');
    
    EEG = pop_eegfiltnew(EEG, 'hicutoff',60);
    dataset_name = convertStringsToChars(sprintf("%s__70", dataset_name));
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'setname',dataset_name,'gui','off');
    
    EEG = pop_eegfiltnew(EEG, 'locutoff',49.9,'hicutoff',50.1,'revfilt',1);
    dataset_name = convertStringsToChars(sprintf("%s__N_50", dataset_name));
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'setname',dataset_name,'gui','off');
    
    
    EEG = pop_clean_rawdata(EEG, 'FlatlineCriterion','off','ChannelCriterion','off','LineNoiseCriterion','off','Highpass','off','BurstCriterion',20,'WindowCriterion',0.25,'BurstRejection','on','Distance','Euclidian','WindowCriterionTolerances',[-Inf 7] );
    EEG = pop_clean_rawdata(EEG, 'FlatlineCriterion','off','ChannelCriterion','off','LineNoiseCriterion','off','Highpass','off','BurstCriterion',20,'WindowCriterion',0.25,'BurstRejection','on','Distance','Euclidian','WindowCriterionTolerances',[-Inf 7] );

    EEG = pop_clean_rawdata(EEG, 'FlatlineCriterion','off','ChannelCriterion','off','LineNoiseCriterion','off','Highpass','off','BurstCriterion',20,'WindowCriterion',0.25,'BurstRejection','off','Distance','Euclidian','WindowCriterionTolerances',[-Inf 7] );

    dataset_name = convertStringsToChars(sprintf("%s__ASR", dataset_name));
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'setname',dataset_name,'gui','off');
    
    dataset_file = convertStringsToChars(sprintf("%s_ASR.set", subject_code));
    EEG = pop_saveset( EEG, 'filename',dataset_file,'filepath', dataset_file_out_path);
    
    EEG = eeg_checkset( EEG );
    dataset_csv_path = convertStringsToChars(sprintf("%scsv/%0.2d.csv", dataset_file_out_path, k));
    fprintf(1, 'SAVING [%s]', dataset_csv_path)
    
    pop_export(EEG,dataset_csv_path,'transpose','on','precision',6);
    
end

eeglab redraw;
