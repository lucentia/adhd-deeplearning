import os
import sys
import mne
import matplotlib.pyplot as plt
import json
import csv
import argparse

import numpy as np
import pandas as pd

from asrpy import ASR
from pyprep.find_noisy_channels import NoisyChannels
from time import perf_counter

from datetime import datetime

dir_path = os.path.dirname(os.path.realpath(__file__))

sys.path.append(os.path.join(dir_path, 'lib', 'JSData'))
sys.path.append(os.path.join(dir_path, 'lib', 'JSPreprocessing'))
sys.path.append(os.path.join(dir_path, 'lib', 'JSLog'))

from JSDataAdhd import JSDataAdhd
from JSPreprocessingAdhd import JSPreprocessingAdhd
from JSLog import JSLog

def compute_psd():
    pass
    # fig=plt.figure()

    # ax1=fig.add_subplot(4, 1, 1)
    # ax2=fig.add_subplot(4, 1, 2)
    # ax3=fig.add_subplot(4, 1, 3)
    # ax4=fig.add_subplot(4, 1, 4)

    # min_y=90
    # max_y=170
    # ax1.set_ylim(min_y, max_y)
    # ax2.set_ylim(min_y, max_y)
    # ax3.set_ylim(min_y, max_y)
    # ax4.set_ylim(min_y, max_y)

    # raw_mne.compute_psd().plot(picks='data', exclude='bads', axes=ax1, show=False)
    # raw_mne_filtered.compute_psd().plot(picks='data', exclude='bads', axes=ax2, show=False)
    # raw_mne_notch.compute_psd().plot(picks='data', exclude='bads', axes=ax3, show=False)
    # raw_mne_asr.compute_psd().plot(picks='data', exclude='bads', axes=ax4, show=False)

    # plt.show()

def plot_eeg(np_data_subject, title, ch_names, id_subject, subject_type_id, preprocessing_type, dir_log):
    
    vspace=600
    color='k'
    seconds = 60
    dest_plot_dir=os.path.join(dir_log, "EEG_PLOTS")
    #dest_plot_dir=os.path.join(base_output_path, preprocessing_type, seconds)
    if not os.path.exists(dest_plot_dir):
        os.mkdir(dest_plot_dir)

    bases = np.flip(vspace * np.arange(19))
    print(bases)

    data = (np_data_subject.T + bases)
    data = data[0:seconds*128, :]

    print("NP_DATA \t- type {}\t- shape {}".format(type(np_data_subject), np_data_subject.shape))
    print("DATA \t- type {}\t- shape {}".format(type(data), data.shape))

    time = np.arange(data.shape[0]) / 128

    fig_a = plt.figure(figsize=[24, 12], dpi=150)
    # Plot EEG versus time
    plt.plot(time, data, color=color, linewidth=0.3)

    # Add gridlines to the plot
    plt.grid()

    # Label the axes
    plt.xlabel('Time (s)')
    plt.ylabel('Channels')
        
    # The y-ticks are set to the locations of the electrodes. The international 10-20 system defines
    # default names for them.
    plt.gca().yaxis.set_ticks(bases)
    plt.gca().yaxis.set_ticklabels(ch_names)
        
    # Put a nice title on top of the plot
    plt.title(title)
    plt.savefig(os.path.join(dest_plot_dir, f"eeg_{id_subject}_{subject_type_id}.png"))
    plt.close()

def get_save_path_file(data_path, freq_label, preproccesing, subject_label, seconds_start, seconds_end):
    
    save_path=os.path.join(data_path, f"py_{freq_label}_{preproccesing}__s_{seconds_start}_{seconds_end}")
    if not os.path.isdir(save_path):
        os.mkdir(save_path)

    if subject_label[1]==0:
        save_path_type=os.path.join(save_path, "CONTROLS")
        if not os.path.isdir(save_path_type):
            os.mkdir(save_path_type)
    else:
        save_path_type=os.path.join(save_path, "ADHD")
        if not os.path.isdir(save_path_type):
            os.mkdir(save_path_type)

    return os.path.join(save_path_type, f"{subject_label[0]:02}.csv")
    
def save_raw_to_csv(raw, data_path, freq_label, preproccesing, subject_label, jsData, seconds_start, seconds_end):
    
    save_path_file=get_save_path_file(data_path, freq_label, preproccesing, subject_label, seconds_start, seconds_end)
    data_raw_mne, time_raw_mne = raw[:]
    
    if preproccesing=='csd':
        data_raw_mne/=1000 # Adjust to microVolts

    ds_raw_mne=pd.DataFrame(data_raw_mne.transpose(), columns=jsData.eeg_info.ch_names)
    ds_raw_mne.insert(0,'Time', time_raw_mne*1000)
    ds_raw_mne.to_csv(save_path_file, index=None, sep='\t')

def preprocessing_pipeline(subject_data, subject_label, jsData, data_path, log_path, cutoff, fmin, fmax, fnotch, csd, seconds_start, seconds_end):

    raw_mne = mne.io.RawArray(subject_data[:,seconds_start*128:seconds_end*128], jsData.eeg_info).load_data()
    print(f"DATA TYPE [{type(raw_mne._data)}] - SHAPE [{raw_mne._data.shape}]")
    # save_raw_to_csv(raw_mne, data_path, "", "original", subject_label, jsData, seconds_start, seconds_end)

    # original_epochs=mne.make_fixed_length_epochs(
    #     raw_mne,
    #     duration=4.0, 
    #     preload=True, 
    #     reject_by_annotation=True, 
    #     proj=True, 
    #     overlap=0.0, 
    #     id=1, 
    #     verbose='INFO').average()


    # plot_eeg(raw_mne._data, "Original", jsData.eeg_info.ch_names, jsData.csv_labels[preprocessing_data_type][75][0], jsData.csv_labels[preprocessing_data_type][75][1], "original", "/home/javier/Documents/Doctorat/adhd-deeplearning/v2")

    # fig_original=original_epochs.plot(spatial_colors=True)
    # print(f"FIGURE TYPE: [{type(fig_original)}]")
    # plt.show()

    try:
        raw_mne_filtered=raw_mne.copy().filter(l_freq=fmin, h_freq=fmax, verbose=False)
        filter_str=f"{str(fmin).replace('.', '_')}_{str(fmax).replace('.', '_')}"
        #save_raw_to_csv(raw_mne_filtered, data_path, filter_str, "filtered", subject_label, jsData, seconds_start, seconds_end)
    except Exception as err:
        print(f"\n###\n\tERROR FILTER [{subject_label[0]}][{subject_label[1]}]{err}\n###\n")
        save_error(f"ERROR FILTER [{subject_label[0]}][{subject_label[1]}] [{fmin}][{fmax}] {err}")

    try:
        raw_mne_notch=raw_mne_filtered.copy().notch_filter(fnotch, verbose=False)
        notch_str=f"{filter_str}_{str(fnotch).replace('.', '_')}"
        save_raw_to_csv(raw_mne_notch, data_path, notch_str, "notch", subject_label, jsData, seconds_start, seconds_end)
        print(f"SFREQ [{raw_mne_notch.info['sfreq']}]")
    except Exception as err:
        print(f"\n###\n\tERROR NOTCH [{subject_label[0]}][{subject_label[1]}]{err}\n###\n")
        save_error(f"ERROR NOTCH [{subject_label[0]}][{subject_label[1]}] [{fmin}][{fmax}] {err}")

    try:
        asr = ASR(sfreq=raw_mne_notch.info["sfreq"], cutoff=cutoff)
        asr.fit(raw_mne_notch)
        raw_mne_asr = asr.transform(raw_mne_notch)
        asr_str=f"{notch_str}_{cutoff}"
        save_raw_to_csv(raw_mne_asr, data_path, asr_str, "asr", subject_label, jsData, seconds_start, seconds_end)
    except Exception as err:
        print(f"\n###\n\tERROR ASR [{subject_label[0]}][{subject_label[1]}]{err}\n###\n")
        save_error(f"ERROR ASR [{subject_label[0]}][{subject_label[1]}] [{fmin}][{fmax}] {err}")


    # try:
    #     raw_csd = mne.preprocessing.compute_current_source_density(raw_mne_asr)
    #     save_raw_to_csv(raw_csd, data_path, asr_str, "csd", subject_label, jsData, seconds_start, seconds_end)
    # except Exception as err:
    #     print(f"\n###\n\tERROR CSD [{subject_label[0]}][{subject_label[1]}]{err}\n###\n")
    #     save_error(f"ERROR CSD [{subject_label[0]}][{subject_label[1]}] [{fmin}][{fmax}] {err}")



def start_asr(experiment_name="ASR", preprocessing_data_type="original", cutoff=5, fmin=5, fmax=45, fnotch=50, csd=True, seconds_start=0, seconds_end=72):
    
    jsLog=JSLog(f"prep_{experiment_name}_{preprocessing_data_type}")
    configuration_path="configuration.json"
    config_data=None
    with open(configuration_path) as f:
        config_data = json.load(f)

    jsData = JSDataAdhd(experiment_name, config_file=configuration_path)
        
    jsData.load_data_type(preprocessing_data_type)


    for subject_data, subject_label in zip(jsData.csv_data[preprocessing_data_type], jsData.csv_labels[preprocessing_data_type]):
        print(f"[{subject_data.shape}], [{subject_label[0]}][{subject_label[1]}]")
        jsLog.w(f"[{subject_data.shape}], [{subject_label[0]}][{subject_label[1]}]")
        try:
            preprocessing_pipeline(subject_data, subject_label, jsData, config_data["base_data_path"], config_data["base_log_path"], cutoff, fmin, fmax, fnotch, csd, seconds_start, seconds_end)
        except Exception as err:
            jsLog.w(f"ERROR [{subject_label[0]}][{subject_label[1]}]\n\t[{type(err)=}] {err}")
            print(err)
            print(subject_label)
            sys.exit()

def start_bad_channels_noise(log_dir, preprocessing_data_type="py_asr_15", HF_zscore_threshold=5.0):
    configuration_path="configuration.json"
    config_data=None
    with open(configuration_path) as f:
        config_data = json.load(f)

    experiment_name="NOISE"
    

    jsData = JSDataAdhd(experiment_name, config_file=configuration_path)
        
    jsData.load_data_type(preprocessing_data_type)
    jsData.load_model_data_raw(preprocessing_data_type, 4.0, 0.75)

    bad_channels_a=[]
    bad_channels_b=[]
    statistics=[]

    with open(f"{log_dir}/{preprocessing_data_type}_bad_channels_noise_{str(HF_zscore_threshold).replace('.', '_')}.csv", "w") as csvfile:
        log_writer=csv.writer(csvfile, delimiter=',')
        log_writer.writerow(["SUBJECT", "TYPE", "BAD_NOISE", "median_channel_noisiness", "channel_noisiness_sd", "hf_noise_zscores"])

        for subject_data, subject_label in zip(jsData.csv_data[preprocessing_data_type], jsData.csv_labels[preprocessing_data_type]):

            try:
                print(f"[{subject_data.shape}], [{subject_label[0]}][{subject_label[1]}]")

                raw_mne = mne.io.RawArray(subject_data, jsData.eeg_info).load_data()
                nd = NoisyChannels(raw_mne)
                nd.find_bad_by_hfnoise(HF_zscore_threshold)
                log_writer.writerow([subject_label[0], subject_label[1], nd.bad_by_hf_noise, nd._extra_info["bad_by_hf_noise"]["median_channel_noisiness"], nd._extra_info["bad_by_hf_noise"]["channel_noisiness_sd"], nd._extra_info["bad_by_hf_noise"]["hf_noise_zscores"]])
            except:
                log_writer.writerow([subject_label[0], subject_label[1], "ERROR"])

    with open(f"{log_dir}/{preprocessing_data_type}_bad_channels_noise_epoch_{str(HF_zscore_threshold).replace('.', '_')}.csv", "w") as csvfile:
        log_writer=csv.writer(csvfile, delimiter=',')
        log_writer.writerow(["SUBJECT", "TYPE", 
                             "median_channel_noisiness", "channel_noisiness_sd", "channel_noisiness_max", "max", "min",
                             "mask_05", "mask_06", "mask_07", "mask_08", "mask_09", "mask_10",
                             "hf_noise_bad_channels",
                             "hf_noise_zscores"])

        for epoch, subject_label in zip(jsData.model_data, jsData.model_labels):

            #try:
            print(f"[{epoch.shape}], [{subject_label[0]}][{subject_label[1]}]")

            raw_mne = mne.io.RawArray(epoch, jsData.eeg_info).load_data()
            nd = NoisyChannels(raw_mne)
            nd.find_bad_by_hfnoise(HF_zscore_threshold)
            reg_statistics=[
                subject_label[0], 
                subject_label[1], 
                nd._extra_info["bad_by_hf_noise"]["median_channel_noisiness"], 
                nd._extra_info["bad_by_hf_noise"]["channel_noisiness_sd"], 
                max(nd._extra_info["bad_by_hf_noise"]["hf_noise_zscores"]),
                0 if max(nd._extra_info["bad_by_hf_noise"]["hf_noise_zscores"]) > 5 else 1,      #05
                0 if max(nd._extra_info["bad_by_hf_noise"]["hf_noise_zscores"]) > 6 else 1,      #06
                0 if max(nd._extra_info["bad_by_hf_noise"]["hf_noise_zscores"]) > 7 else 1,      #07
                0 if max(nd._extra_info["bad_by_hf_noise"]["hf_noise_zscores"]) > 8 else 1,      #08
                0 if max(nd._extra_info["bad_by_hf_noise"]["hf_noise_zscores"]) > 9 else 1,      #09
                0 if max(nd._extra_info["bad_by_hf_noise"]["hf_noise_zscores"]) > 10 else 1,     #10
                epoch.max(),
                epoch.min()]
            
            log_writer.writerow(reg_statistics)
            statistics.append(reg_statistics)
            #except:
            #    log_writer.writerow([subject_label[0], subject_label[1], "ERROR"])


    return np.asarray(statistics)

def start_bad_channels(preprocessing_data_type="py_asr_15", experiment_name="ASR"):
    configuration_path="configuration.json"
    config_data=None
    with open(configuration_path) as f:
        config_data = json.load(f)

    
    

    jsData = JSDataAdhd(experiment_name, config_file=configuration_path)
        
    jsData.load_data_type(preprocessing_data_type)

    bad_channels_a=[]
    bad_channels_b=[]

    for subject_data, subject_label in zip(jsData.csv_data[preprocessing_data_type], jsData.csv_labels[preprocessing_data_type]):

        try:
            print(f"[{subject_data.shape}], [{subject_label[0]}][{subject_label[1]}]")

            raw_mne = mne.io.RawArray(subject_data, jsData.eeg_info).load_data()
            nd = NoisyChannels(raw_mne)
            nd2 = NoisyChannels(raw_mne)

            start_time = perf_counter()
            nd.find_all_bads(channel_wise=True)
            print("--- %s seconds ---" % (perf_counter() - start_time))

            # Repeat channel-wise RANSAC using a single channel at a time. This is slower
            # but needs less memory.
            #start_time = perf_counter()
            #nd2.find_all_bads(channel_wise=True, max_chunk_size=1)
            #print("--- %s seconds ---" % (perf_counter() - start_time))

            bad_channs=nd.get_bads(verbose=True)
            #bad_channs=nd2.get_bads(verbose=True)

            # Check channels that go bad together by correlation (RANSAC)
            print(f"[{subject_label[0]}][{subject_label[1]}] - {nd.get_bads()}")

            # Check that the channel wise RANSAC yields identical results
            #print(f"[{subject_label[0]}][{subject_label[1]}] - {nd2.get_bads()}")

            bad_channels_a.append({"label":(subject_label[0], subject_label[1]), "list_bad":nd.get_bads(), "object":nd })
            #bad_channels_b.append({"label":(subject_label[0], subject_label[1]), "list_bad":nd2.get_bads(), "object":nd2 })
        except:
            bad_channels_a.append({"label":(subject_label[0], subject_label[1]), "list_bad":None, "object":None })
        
    return bad_channels_a #, bad_channels_b



def gen_band_eeg_signal(preprocessing_data_type="original"):
    configuration_path="configuration.json"
    config_data=None
    with open(configuration_path) as f:
        config_data = json.load(f)

    experiment_name="ASR"
    

    jsData = JSDataAdhd(experiment_name, config_file=configuration_path)
        
    jsData.load_data_type(preprocessing_data_type)

    bad_channels_a=[]
    bad_channels_b=[]

    for subject_data, subject_label in zip(jsData.csv_data[preprocessing_data_type], jsData.csv_labels[preprocessing_data_type]):
        
        raw_mne = mne.io.RawArray(subject_data, jsData.eeg_info).load_data()
        print(f"DATA TYPE [{type(raw_mne._data)}] - SHAPE [{raw_mne._data.shape}]")

        for fmin_max in [(1,4), (4,8), (8,12), (12, 30), (30,60)]:
            try:
                preprocessing_pipeline(subject_data, subject_label, jsData, data_path, log_path, cutoff, fmin_max[0], fmin_max[1], 50, False, 0, 10000)
            except Exception as err:
                print(f"\n###\n\tERROR FILTER [{subject_label[0]}][{subject_label[1]}]{err}\n###\n")
                save_error(f"ERROR FILTER [{subject_label[0]}][{subject_label[1]}] [{fmin}][{fmax}] {err}")



def save_error(text):
    configuration_path="configuration.json"
    config_data=None
    with open(configuration_path) as f:
        config_data = json.load(f)

    with open(os.path.join(config_data["base_data_path"], "error.log"), "a") as fp_error:
        
        fp_error.write(f"[{datetime.now().strftime('%d/%m/%Y %H:%M:%S')}] {text}\n")

if __name__=="__main__":
    
    # start_asr(experiment_name="ASR_CSD_5_20", preprocessing_data_type="original", cutoff=5, fmin=5, fmax=20, fnotch=50, csd=True)
    #start_asr(experiment_name="ASR_CSD_5_45", preprocessing_data_type="original", cutoff=5, fmin=5, fmax=45, fnotch=50, csd=True)
    #start_asr(experiment_name="ASR_CSD_4_60", preprocessing_data_type="original", cutoff=5, fmin=4, fmax=60, fnotch=50, csd=True)
    #start_asr(experiment_name="ASR_CSD_1_4", preprocessing_data_type="original", cutoff=5, fmin=1, fmax=4, fnotch=50, csd=True, seconds_start=5, seconds_end=120)
    #start_asr(experiment_name="ASR_CSD_4_8", preprocessing_data_type="original", cutoff=5, fmin=4, fmax=8, fnotch=50, csd=True, seconds_start=5, seconds_end=120)
    #start_asr(experiment_name="ASR_CSD_8_12", preprocessing_data_type="original", cutoff=5, fmin=8, fmax=12, fnotch=50, csd=True, seconds_start=5, seconds_end=120)
    #start_asr(experiment_name="ASR_CSD_12_30", preprocessing_data_type="original", cutoff=5, fmin=12, fmax=30, fnotch=50, csd=True, seconds_start=5, seconds_end=120)
    #start_asr(experiment_name="ASR_CSD_30_60", preprocessing_data_type="original", cutoff=5, fmin=30, fmax=60, fnotch=50, csd=True, seconds_start=5, seconds_end=120)

    #sys.exit()
    #start_asr(experiment_name="ASR_CSD", preprocessing_data_type="original", cutoff=5, fmin=float(args.fmin), fmax=float(args.fmax), fnotch=50, csd=True)

    # start_bad_channels_noise(preprocessing_data_type="py_asr_15", HF_zscore_threshold=10.0)
    # sys.exit()

    log_dir="/Volumes/extJavier/DOCTORAT/logs/adhd-deeplearning-bads"
    preprocessing_data_type="preprocessed_asr_04"
    HF_zscore_threshold=10.0
    statistics=start_bad_channels_noise(log_dir, preprocessing_data_type, HF_zscore_threshold)

    print(statistics)

    print(f"LEN: {len(statistics)} - OUTLIERS 10: [{sum(statistics[:,12])} - OUTLIERS 09: [{sum(statistics[:,11])} - OUTLIERS 08: [{sum(statistics[:,9])}] - OUTLIERS 07: [{sum(statistics[:,8])}] - OUTLIERS 06: [{sum(statistics[:,7])}] - OUTLIERS 05: [{sum(statistics[:,8])}]")

    # bad_channels_a=start_bad_channels(preprocessing_data_type, "Checking Bad Channels")
    # output_log_filename=os.path.join(log_dir, f"{preprocessing_data_type}_bad_channels.csv")
    # 
    # 
    # with open(output_log_filename, "w") as csvfile:
    #     log_writer=csv.writer(csvfile, delimiter=',')
    #     log_writer.writerow(["SUBJECT", "TYPE", "BAD_CHANS", "BAD_RANSAC", "BAD_FLAT", "BAD_NOISE"])
    #     for item in bad_channels_a:
    # 
    #         if item['list_bad'] != None and item['object'] != None:
    #             print(f"SUBJECT: {item['label']}\tBAD CHANS: {item['list_bad']}\n\tBAD RANSAC: {item['object'].bad_by_ransac}\n\tBAD FLAT  : {item['object'].bad_by_flat}\n\tBAD SNR   : {item['object'].bad_by_SNR}")
    # 
    #             log_writer.writerow([item['label'][0], item['label'][1], item['list_bad'], item['object'].bad_by_ransac, item['object'].bad_by_flat, item['object'].bad_by_SNR])
    #         else:
    #             log_writer.writerow([item['label'][0], item['label'][1], "ERROR", "ERROR", "ERROR", "ERROR"])
                

        
