import os
import sys
import json
import mne
import re

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from mne.time_frequency import tfr_stockwell, tfr_array_stockwell

def get_diff_length(dir_data, dir_log):
    pass
def plot_all(dir_data, dir_log, preprocessing_type):

    for subject_type, subject_type_id in zip(['CONTROLS', 'ADHD'], [0, 1]):
            
        data_path = os.path.join(dir_data, preprocessing_type, subject_type)
        files = sorted(os.listdir(data_path))
        np_data_list = []

        for file in files:
            
            m=re.match(r"^(\d{2}).(csv)$", file)

            if m:
                
                id_subject=int(m.group(1))
                print(f"[{data_path}] - [{id_subject}] - [{file}]")
                asr_data_path = os.path.join(data_path, file)
                print(asr_data_path)
                ds = pd.read_csv(asr_data_path, sep='\t')
                np_data_subject = ds.drop(columns=['Time']).to_numpy().transpose()
                plot_eeg(np_data_subject, f"EEG DATA ASR {subject_type} {id_subject}", montage.ch_names, id_subject, subject_type_id, preprocessing_type, dir_log)

def plot_eeg(np_data_subject, title, ch_names, id_subject, subject_type_id, preprocessing_type, dir_log):
    
    
    vspace=600
    color='k'
    seconds = 60
    base_output_path=os.path.join(dir_log, "EEG_PLOTS")
    dest_plot_dir=os.path.join(base_output_path, preprocessing_type, seconds)
    if not os.path.exists(dest_plot_dir):
        os.mkdir(dest_plot_dir)

    bases = np.flip(vspace * np.arange(19))
    print(bases)

    data = (np_data_subject.T + bases)
    data = data[0:seconds*128, :]

    print("NP_DATA \t- type {}\t- shape {}".format(type(np_data_subject), np_data_subject.shape))
    print("DATA \t- type {}\t- shape {}".format(type(data), data.shape))

    time = np.arange(data.shape[0]) / 128

    fig_a = plt.figure(figsize=[24, 12], dpi=150)
    # Plot EEG versus time
    plt.plot(time, data, color=color, linewidth=0.3)

    # Add gridlines to the plot
    plt.grid()

    # Label the axes
    plt.xlabel('Time (s)')
    plt.ylabel('Channels')
        
    # The y-ticks are set to the locations of the electrodes. The international 10-20 system defines
    # default names for them.
    plt.gca().yaxis.set_ticks(bases)
    plt.gca().yaxis.set_ticklabels(ch_names)
        
    # Put a nice title on top of the plot
    plt.title(title)
    plt.savefig(os.path.join(dest_plot_dir, "eeg_{id_subject}_{subject_type_id}.png"))
    plt.close()

sampling_frequency = 128

if __name__ == "__main__":

    configuration_path = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'configuration.json')
        
    with open(configuration_path) as f:
        config_data = json.load(f)

    dir_path = os.path.dirname(os.path.realpath(__file__))
    dir_data = config_data['base_data_path']
    dir_log = config_data['base_log_path']
    print(dir_data)
    print(dir_log)

    ####

    montage_file_name = os.path.join(dir_data, 'montage', 'Standard-10-20-Cap19new.loc')
    print("LOADING MONTAGE [{}]".format(montage_file_name))

    montage = mne.channels.read_custom_montage(
        fname=montage_file_name,
        coord_frame='head')

    print(montage.ch_names)

    ####

    eeg_info = mne.create_info(
                ch_names=montage.ch_names,
                ch_types=['eeg'] * len(montage.ch_names),
                sfreq=sampling_frequency)


    ####



sys.exit()
asr_data_path = os.path.join(dir_data, 'preprocessed_asr_04', 'ADHD', '16.csv')
print(asr_data_path)
ds = pd.read_csv(asr_data_path, sep='\t')
np_data_subject = ds.drop(columns=['Time']).to_numpy().transpose()
plot_eeg(np_data_subject, "EEG DATA ASR CONTROL 11", montage.ch_names)

sys.exit()

####

original_data_path = os.path.join(dir_data, 'original', 'ADHD', '16.csv')
print(original_data_path)
ds = pd.read_csv(original_data_path, sep='\t')
original_np_data_subject = ds.drop(columns=['Time']).to_numpy().transpose()
plot_eeg(original_np_data_subject, "EEG DATA ORIGINAL CONTROL 11", montage.ch_names)

####

filtered_data_path = os.path.join(dir_data, 'preprocessed_filter', 'ADHD', '16.csv')
print(filtered_data_path)
ds = pd.read_csv(filtered_data_path, sep='\t')
np_data_subject = ds.drop(columns=['Time']).to_numpy().transpose()
plot_eeg(np_data_subject, "EEG DATA FILTERED CONTROL 11", montage.ch_names)


raw = mne.io.RawArray(
            original_np_data_subject/10**7, eeg_info)

raw.set_montage(montage)
# raw.plot_psd()
raw.plot()
new_events = mne.make_fixed_length_events(raw, duration=4.)
epochs = mne.Epochs(raw, new_events, tmin=-1., tmax=3.)

####

## mne_data.plot_psd_topomap()
epochs.plot_psd_topomap()

power, itc = tfr_stockwell(epochs, fmin=1., fmax=60., decim=4, n_jobs=1,
                           width=1, return_itc=True)

print("POWER SHAPE: {}".format)

power.plot([1], baseline=(-0.5, 0), title='S-transform (power)')

itc.plot([1], baseline=None, title='S-transform (ITC)')

plt.show()

sys.exit()
####



picks = mne.pick_types(mne_data.info, meg=False, eeg=True, exclude=[])
start, stop = mne_data.time_as_index([0, 10])

n_channels = 19
data, times = mne_data[picks[:n_channels], start:stop]
ch_names = [mne_data.info['ch_names'][p] for p in picks[:n_channels]]

print("DATA \t\t- type {}\t- shape {}{}\t- sample [2,20][{}][{}]".format(type(data), data.shape, np_data_subject.shape, data[2,20], np_data_subject[2,20]))
print("TIMES \t\t- type {}\t- shape {}\t - values {}".format(type(times), times.shape, times))
print("CH_NAMES \t- type {}\t- shape".format(type(ch_names), len(ch_names)))

# plt.figure(figsize=(15,3))
# plt.plot(data)



fig_b = plt.figure()
data_f = (np.flip(np_data_subject.T, 1) + bases)
data_f = data_f[0:seconds*128, :]

plt.plot(time, data_f, color=color, linewidth=0.3)

# Add gridlines to the plot
plt.grid()

# Label the axes
plt.xlabel('Time (s)')
plt.ylabel('Channels')
    
# The y-ticks are set to the locations of the electrodes. The international 10-20 system defines
# default names for them.
plt.gca().yaxis.set_ticks(bases)
plt.gca().yaxis.set_ticklabels(ch_names[::-1])
    
# Put a nice title on top of the plot
plt.title('EEG data Fliped')

d = dict(mag=1e-12, grad=4e-11, eeg=750e-6, eog=150e-6, ecg=5e-4,
            emg=1e-3, ref_meg=1e-12, misc=1e-3, stim=1,
            resp=1, chpi=1e-4, whitened=1e2)
mne_data.plot(show_scrollbars=False, show_scalebars=True, duration=seconds, scalings=d)

plt.show()