import json
import sys

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import os
import tarfile
import ftplib

class JSTarSend():
    def __init__(self):
        pass

    def send(self, dir_name, tar_file_name, remote_file_name, host, user, pwd):

        with tarfile.open(tar_file_name, mode='w:tar') as browser_tar:
            browser_tar.add(dir_name, arcname=".")

        try:
            ftp_server = ftplib.FTP(host, user, pwd)
            # force UTF-8 encoding
            ftp_server.encoding = "utf-8"
            connected = True
        except ftplib.all_errors as e:
            print(e)
            errorInfo = str(e).split(None, 1)[0]
            print(errorInfo)
            connected = False

        if connected:
            # Read file in binary mode
            with open(tar_file_name, "rb") as file:
                # Command for Uploading the file "STOR filename"
                ftpResponseMessage = ftp_server.storbinary(f"STOR {remote_file_name}", file)

            ftp_server.quit()
          

if __name__ == "__main__":
    pass



       