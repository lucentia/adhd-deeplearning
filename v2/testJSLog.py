import os
import sys

import pandas as pd
import numpy as np


dir_path = os.path.dirname(os.path.realpath(__file__))

sys.path.append(os.path.join(dir_path, 'lib', 'JSData'))
sys.path.append(os.path.join(dir_path, 'lib', 'JSPreprocessing'))
sys.path.append(os.path.join(dir_path, 'lib', 'JSLog'))

from JSDataAdhd import JSDataAdhd
from JSPreprocessingAdhd import JSPreprocessingAdhd
from JSLog import JSLog

output_results_by_subject_filename=\
    "/Volumes/extJavier/DOCTORAT/logs/adhd_deeplearning/MultiScaleCNN_18_02/\
results/results_by_subject.csv"

output_csv=\
    "/Volumes/extJavier/DOCTORAT/logs/adhd_deeplearning/MultiScaleCNN_18_02/\
results_by_subject_c.csv"

output_csv_g=\
    "/Volumes/extJavier/DOCTORAT/logs/adhd_deeplearning/MultiScaleCNN_18_02/\
results_by_subject_c_g.csv"

print(output_results_by_subject_filename)

data=pd.read_csv(output_results_by_subject_filename)[
    ['ix_iteration','ix_fold','subject_step',
    'index_subject', 'type_subject','value']]

print(data.head())

data['value_computed'] = np.where(data['value'] > 0.5, 1, 0)

print(data.head())

data['value_computed_ok'] = np.where(data['type_subject']==data['value_computed'], 1, 0)

print(data.head())

data.to_csv(output_csv)

data_grouped=data.groupby(
    ['ix_iteration', 
    'ix_fold', 
    'index_subject', 
    'type_subject']).agg({'value_computed_ok':['sum', 'count']})

data_grouped.to_csv(output_csv_g)   