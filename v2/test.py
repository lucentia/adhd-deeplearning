import os
import sys
import gc
import time
import ast

import numpy as np
import tensorflow as tf


from sklearn.model_selection import KFold
from sklearn.utils import shuffle
from tensorflow import keras

dir_path = os.path.dirname(os.path.realpath(__file__))

sys.path.append(os.path.join(dir_path, 'lib', 'JSData'))
sys.path.append(os.path.join(dir_path, 'lib', 'JSPreprocessing'))
sys.path.append(os.path.join(dir_path, 'lib', 'JSLog'))

from JSDataAdhd import JSDataAdhd
from JSPreprocessingAdhd import JSPreprocessingAdhd
from JSLog import JSLog

from JSADHDModels import MultiScaleCNN, EEGNet,MultiScaleCNN_ParallelBlock, MultiDimensionalCNN, MultiScale_Recurrent, MultiScaleCNN_Parallel_LSTM, MultiScaleCNN_Parallel_CONVLSTM, MultiScaleCNN_Parallel_CONV_LSTM_INPUT, MultiScaleCNN_Parallel_CONV_LSTM_OUTPUT


from CsvModelParams import CsvModelParams

# p = CsvModelParams("SPCnnModel", 11)
p = CsvModelParams("MultiScaleCNN_Parallel_CONV_LSTM_OUTPUT", 1)
# p = CsvModelParams("EEGNet", 1)

print(p.parameters)
#print(p.parameters["activation_conv_list"])
#list_js=ast.literal_eval(p.parameters["activation_conv_list"])
#print(list_js)

# print(p.parameters["num_epochs"])
# print(p.parameters["s_transform_img_width"])
# print(p.parameters["s_transform_img_height"])

# model = SPCnnModelBn(p)
# model.build(input_shape=(None, int(p.parameters["s_transform_img_height"]), int(p.parameters["s_transform_img_width"]),19))
# model.model(shape=(int(p.parameters["s_transform_img_height"]), int(p.parameters["s_transform_img_width"]),19)).summary()

seconds=2
model=MultiScaleCNN_Parallel_CONV_LSTM_OUTPUT(p)
model.build(input_shape=(None, 19, 128*seconds))
model.model(shape=(19, 128*seconds)).summary()
