import os
import sys
import mne
import matplotlib.pyplot as plt
import json
import csv


import numpy as np
import pandas as pd

from asrpy import ASR
from pyprep.find_noisy_channels import NoisyChannels
from pyprep.prep_pipeline import PrepPipeline
from os import path

from time import perf_counter

dir_path = os.path.dirname(os.path.realpath(__file__))

sys.path.append(os.path.join(dir_path, 'lib', 'JSData'))
sys.path.append(os.path.join(dir_path, 'lib', 'JSPreprocessing'))
sys.path.append(os.path.join(dir_path, 'lib', 'JSLog'))

from JSDataAdhd import JSDataAdhd
from JSPreprocessingAdhd import JSPreprocessingAdhd

def compute_psd():
    pass
    # fig=plt.figure()

    # ax1=fig.add_subplot(4, 1, 1)
    # ax2=fig.add_subplot(4, 1, 2)
    # ax3=fig.add_subplot(4, 1, 3)
    # ax4=fig.add_subplot(4, 1, 4)

    # min_y=90
    # max_y=170
    # ax1.set_ylim(min_y, max_y)
    # ax2.set_ylim(min_y, max_y)
    # ax3.set_ylim(min_y, max_y)
    # ax4.set_ylim(min_y, max_y)

    # raw_mne.compute_psd().plot(picks='data', exclude='bads', axes=ax1, show=False)
    # raw_mne_filtered.compute_psd().plot(picks='data', exclude='bads', axes=ax2, show=False)
    # raw_mne_notch.compute_psd().plot(picks='data', exclude='bads', axes=ax3, show=False)
    # raw_mne_asr.compute_psd().plot(picks='data', exclude='bads', axes=ax4, show=False)

    # plt.show()

def plot_eeg(np_data_subject, title, ch_names, id_subject, subject_type_id, preprocessing_type, dir_log):
    
    vspace=600
    color='k'
    seconds = 60
    dest_plot_dir=os.path.join(dir_log, preprocessing_type)
    #dest_plot_dir=os.path.join(base_output_path, preprocessing_type, seconds)
    if not os.path.exists(dest_plot_dir):
        os.mkdir(dest_plot_dir)

    bases = np.flip(vspace * np.arange(19))
    print(bases)

    data = (np_data_subject.T + bases)
    data = data[0:seconds*128, :]

    print("NP_DATA \t- type {}\t- shape {}".format(type(np_data_subject), np_data_subject.shape))
    print("DATA \t- type {}\t- shape {}".format(type(data), data.shape))

    time = np.arange(data.shape[0]) / 128

    fig_a = plt.figure(figsize=[24, 12], dpi=150)
    # Plot EEG versus time
    plt.plot(time, data, color=color, linewidth=0.3)

    # Add gridlines to the plot
    plt.grid()

    # Label the axes
    plt.xlabel('Time (s)')
    plt.ylabel('Channels')
        
    # The y-ticks are set to the locations of the electrodes. The international 10-20 system defines
    # default names for them.
    plt.gca().yaxis.set_ticks(bases)
    plt.gca().yaxis.set_ticklabels(ch_names)
        
    # Put a nice title on top of the plot
    plt.title(title)
    plt.savefig(os.path.join(dest_plot_dir, f"eeg_{id_subject}_{subject_type_id}.png"))
    print(f'SAVED: [{os.path.join(dest_plot_dir, f"eeg_{id_subject}_{subject_type_id}.png")}]')
    plt.close()

def get_save_path_file(data_path, cutoff, preproccesing, subject_label):
    
    save_path=os.path.join(data_path, f"py_{preproccesing}_{cutoff}")
    if not os.path.isdir(save_path):
        os.mkdir(save_path)

    if subject_label[1]==0:
        save_path_type=os.path.join(save_path, "CONTROLS")
        if not os.path.isdir(save_path_type):
            os.mkdir(save_path_type)
    else:
        save_path_type=os.path.join(save_path, "ADHD")
        if not os.path.isdir(save_path_type):
            os.mkdir(save_path_type)

    return os.path.join(save_path_type, f"{subject_label[0]:02}.csv")
    
def save_raw_to_csv(raw, data_path, cutoff, preproccesing, subject_label, jsData):
    
    save_path_file=get_save_path_file(data_path, cutoff, preproccesing, subject_label)
    data_raw_mne, time_raw_mne = raw[:]
    ds_raw_mne=pd.DataFrame(data_raw_mne.transpose(), columns=jsData.eeg_info.ch_names)
    ds_raw_mne.insert(0,'Time', time_raw_mne*1000)
    ds_raw_mne.to_csv(save_path_file, index=None, sep='\t')

def preprocessing_pipeline(subject_data, subject_label, jsData, data_path, log_path, cutoff=5):

    raw_mne = mne.io.RawArray(subject_data, jsData.eeg_info).load_data()
    print(f"DATA TYPE [{type(raw_mne._data)}] - SHAPE [{raw_mne._data.shape}]")
    save_raw_to_csv(raw_mne, data_path, "", "original", subject_label, jsData)

    # original_epochs=mne.make_fixed_length_epochs(
    #     raw_mne,
    #     duration=4.0, 
    #     preload=True, 
    #     reject_by_annotation=True, 
    #     proj=True, 
    #     overlap=0.0, 
    #     id=1, 
    #     verbose='INFO').average()


    # plot_eeg(raw_mne._data, "Original", jsData.eeg_info.ch_names, jsData.csv_labels[preprocessing_data_type][75][0], jsData.csv_labels[preprocessing_data_type][75][1], "original", "/home/javier/Documents/Doctorat/adhd-deeplearning/v2")

    # fig_original=original_epochs.plot(spatial_colors=True)
    # print(f"FIGURE TYPE: [{type(fig_original)}]")
    # plt.show()

    raw_mne_filtered=raw_mne.copy().filter(l_freq=2., h_freq=60.)
    save_raw_to_csv(raw_mne_filtered, data_path, "02_60", "filtered", subject_label, jsData)

    raw_mne_notch=raw_mne_filtered.copy().notch_filter(50.)
    save_raw_to_csv(raw_mne_notch, data_path, "50", "notch", subject_label, jsData)

    print(f"SFREQ [{raw_mne_notch.info['sfreq']}]")
    
    asr = ASR(sfreq=raw_mne_notch.info["sfreq"], cutoff=cutoff)
    asr.fit(raw_mne_notch)
    raw_mne_asr = asr.transform(raw_mne_notch)
    save_raw_to_csv(raw_mne_asr, data_path, cutoff, "asr", subject_label, jsData)

def start_asr(cutoff=5):
    configuration_path="configuration_local.json"
    config_data=None
    with open(configuration_path) as f:
        config_data = json.load(f)

    experiment_name="ASR"
    preprocessing_data_type="original"

    jsData = JSDataAdhd(experiment_name, config_file=configuration_path)
        
    jsData.load_data_type(preprocessing_data_type)


    for subject_data, subject_label in zip(jsData.csv_data[preprocessing_data_type], jsData.csv_labels[preprocessing_data_type]):
        print(f"[{subject_data.shape}], [{subject_label[0]}][{subject_label[1]}]")

        try:
            preprocessing_pipeline(subject_data, subject_label, jsData, config_data["base_data_path"], config_data["base_log_path"], cutoff)
        except ValueError as err:
            print(err.__str__)
            print(subject_label)

def start_bad_channels_noise(preprocessing_data_type="py_asr_15", HF_zscore_threshold=5.0):
    configuration_path="configuration_local.json"
    config_data=None
    with open(configuration_path) as f:
        config_data = json.load(f)

    experiment_name="ASR"
    

    jsData = JSDataAdhd(experiment_name, config_file=configuration_path)
        
    jsData.load_data_type(preprocessing_data_type)

    bad_channels_a=[]
    bad_channels_b=[]

    with open(f"{preprocessing_data_type}_bad_channels_noise_{str(HF_zscore_threshold).replace('.', '_')}.csv", "w") as csvfile:
        log_writer=csv.writer(csvfile, delimiter=',')
        log_writer.writerow(["SUBJECT", "TYPE", "BAD_NOISE", "median_channel_noisiness", "channel_noisiness_sd", "hf_noise_zscores"])

        for subject_data, subject_label in zip(jsData.csv_data[preprocessing_data_type], jsData.csv_labels[preprocessing_data_type]):

            try:
                print(f"[{subject_data.shape}], [{subject_label[0]}][{subject_label[1]}]")

                raw_mne = mne.io.RawArray(subject_data, jsData.eeg_info).load_data()
                nd = NoisyChannels(raw_mne)
                nd.find_bad_by_hfnoise(HF_zscore_threshold)
                log_writer.writerow([subject_label[0], subject_label[1], nd.bad_by_hf_noise, nd._extra_info["bad_by_hf_noise"]["median_channel_noisiness"], nd._extra_info["bad_by_hf_noise"]["channel_noisiness_sd"], nd._extra_info["bad_by_hf_noise"]["hf_noise_zscores"]])
            except:
                log_writer.writerow([subject_label[0], subject_label[1], "ERROR"])

def start_pyprep(preprocessing_data_type:str="original", dest_tata_type:str="original_interpolated", line_freqs:list=[]):
    configuration_path="configuration_local.json"
    config_data=None
    with open(configuration_path) as f:
        config_data = json.load(f)

    experiment_name="PyPREP"
    

    jsData = JSDataAdhd(experiment_name, config_file=configuration_path)
        
    jsData.load_data_type(preprocessing_data_type)

    bad_channels_a=[]
    bad_channels_b=[]

    for subject_data, subject_label in zip(jsData.csv_data[preprocessing_data_type], jsData.csv_labels[preprocessing_data_type]):

        try:
            print(f"[{subject_data.shape}], [{subject_label[0]}][{subject_label[1]}]")

            raw_mne = mne.io.RawArray(subject_data, jsData.eeg_info).load_data()
            raw_copy=raw_mne.copy()
            # Fit prep
            prep_params = {
                "ref_chs": "eeg",
                "reref_chs": "eeg",
                "line_freqs": line_freqs
            }

            prep = PrepPipeline(raw_copy, prep_params, jsData.montage)
            prep.fit()
            save_raw_to_csv(prep.raw_eeg, config_data["base_data_path"], 0, dest_tata_type, subject_label, jsData)
            
            jsData.jsLog.w(f"[{subject_label[0]}][{subject_label[1]}] Bad channels: {prep.interpolated_channels}")
            jsData.jsLog.w(f"[{subject_label[0]}][{subject_label[1]}] Bad channels original: {prep.noisy_channels_original['bad_all']}")
            jsData.jsLog.w(f"[{subject_label[0]}][{subject_label[1]}] Bad channels after interpolation: {prep.still_noisy_channels}")

            
        except Exception as err:
            jsData.jsLog.w(f"ERROR [{subject_label[0]}][{subject_label[1]}] {err}. Saving Original.")
            save_raw_to_csv(raw_mne, config_data["base_data_path"], 0, dest_tata_type, subject_label, jsData)
            
        
    return bad_channels_a #, bad_channels_b

def start_csd(preprocessing_data_type:str="original", dest_tata_type:str="original_csd"):
    configuration_path="configuration_local.json"
    config_data=None
    with open(configuration_path) as f:
        config_data = json.load(f)

    experiment_name="csd"
    
    jsData = JSDataAdhd(experiment_name, config_file=configuration_path)
        
    jsData.load_data_type(preprocessing_data_type)

    bad_channels_a=[]
    bad_channels_b=[]

    for subject_data, subject_label in zip(jsData.csv_data[preprocessing_data_type], jsData.csv_labels[preprocessing_data_type]):

        try:
            raw_mne = mne.io.RawArray(subject_data, jsData.eeg_info).load_data()
            raw_csd = mne.preprocessing.compute_current_source_density(raw_mne)

            plot_eeg(
                raw_mne[:][0], 
                f"ASR [{subject_label[0]}][{subject_label[1]}]", 
                raw_mne.info.ch_names, 
                subject_label[0], 
                subject_label[1], 
                preprocessing_data_type, 
                path.join(config_data["base_data_path"], "eeg_plots"))
            
            save_raw_to_csv(raw_csd, config_data["base_data_path"], 0, dest_tata_type, subject_label, jsData)
            plot_eeg(
                raw_csd[:][0]/1000, 
                f"CSD [{subject_label[0]}][{subject_label[1]}]", 
                raw_mne.info.ch_names, 
                subject_label[0], 
                subject_label[1], 
                dest_tata_type, 
                path.join(config_data["base_data_path"], "eeg_plots"))




        except Exception as err:
            jsData.jsLog.w(f"ERROR [{subject_label[0]}][{subject_label[1]}] {err}. Saving Original.")
            save_raw_to_csv(raw_mne, config_data["base_data_path"], 0, dest_tata_type, subject_label, jsData)
        

if __name__=="__main__":
    #start_pyprep("preprocessed_asr_04", "preprocessed_asr_04_interpolated", [50])

    start_csd("preprocessed_asr_04", "preprocessed_asr_04_csd")
