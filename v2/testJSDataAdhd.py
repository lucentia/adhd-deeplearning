
import os
import sys
import getopt
import argparse
import gc
import time

import numpy as np
import tensorflow as tf


from sklearn.model_selection import KFold
from sklearn.utils import shuffle
from tensorflow import keras

dir_path = os.path.dirname(os.path.realpath(__file__))

sys.path.append(os.path.join(dir_path, 'lib', 'JSData'))
sys.path.append(os.path.join(dir_path, 'lib', 'JSPreprocessing'))
sys.path.append(os.path.join(dir_path, 'lib', 'JSLog'))

from JSDataAdhd import JSDataAdhd
from JSPreprocessingAdhd import JSPreprocessingAdhd


def main(
    name:str="statistics",
    preprocessing_data_type:str='preprocessed_asr_04', 
    window_size:float=2.,
    overlapping:float=0.5,
    standarize:bool=True,
    truncate:int=-1):

    print(f"{name} - {preprocessing_data_type} - {window_size} - {overlapping} - {standarize}")

    jsData = JSDataAdhd(name)
    # jsData.load_data_type(preprocessing_data_type)
    # jsData.save_stats_data_type(preprocessing_data_type)
    # jsData.load_model_data_raw(preprocessing_data_type, window_size=window_size, overlapping=overlapping, standarize=standarize, truncate=truncate)

    # jsData.plot_subject(preprocessing_data_type, 31, 0)
    jsData.load_data()
    #print(jsData.model_data.shape)
    #print(jsData.model_labels.shape)
    sys.exit()

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        prog = 'ProgramName',
        description = 'What the program does',
        epilog = 'Text at the bottom of help')

    parser.add_argument('-n', '--name', type=str, default='test')
    parser.add_argument('-p', '--preprocessingtype', type=str, default='preprocessed_asr_04')        
    parser.add_argument('-w', '--windowsize', type=float, default=4.0)               
    parser.add_argument('-o', '--overlapping', type=float, default=0.75)
    parser.add_argument('-s', '--standarize', type=int, default=1)
    parser.add_argument('-t', '--truncate', type=int, default=-1)

    args = parser.parse_args()

    main(args.name, args.preprocessingtype, float(args.windowsize), float(args.overlapping), bool(int(args.standarize)), int(args.truncate))
    


    