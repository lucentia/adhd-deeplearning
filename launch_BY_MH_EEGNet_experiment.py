# -*- coding: utf-8 -*-

import os
import sys

dir_path = os.path.dirname(os.path.realpath(__file__))

sys.path.append(dir_path + '/lib/JSLog')
sys.path.append(dir_path + '/lib/JSData')
sys.path.append(dir_path + '/lib/JSModels')
sys.path.append(dir_path + '/lib/JSExperiment')
sys.path.append(dir_path + '/lib/JSPreprocessing')

from JSExperiment_MH_EEGNet import JSExperiment_MH_EEGNet

def main():
    JSExperiment_MH_EEGNet()

if __name__ == "__main__":
    main()