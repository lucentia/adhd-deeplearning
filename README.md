python3 -m venv venv

source ./venv/bin/activate

pip install -r requirements.txt

python launch_BY_MLP_03_experiment.py

python launch_BY_VGGNet_experiment.py

python launch_BY_EEGNet_experiment.py

python launch_BY_MH_EEGNet_experiment.py

python launch_BY_MH_DSCNet.py