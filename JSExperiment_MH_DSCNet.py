# -*- coding: utf-8 -*-

import random
import os
from tabnanny import verbose


import numpy as np
import tensorflow as tf

from sklearn.model_selection import KFold

from JSLog import JSLog
from JSData import JSData
from JSModels import JSModel_MH_DSCNet
from JSExperiment import JSExperiment

def JSExperiment_MH_DSCNet():
    int_seed = 1234
    str_seed = '1234'

    os.environ['PYTHONHASHSEED'] = str_seed
    os.environ['TF_DETERMINISTIC_OPS'] = '0'

    np.random.seed(int_seed)
    random.seed(int_seed)
    tf.random.set_seed(int_seed)

    jsExperiment = JSExperiment(
        "Multihead SeparableConv1D with automatic clean data with ASR. Parallel model with diferent kernel sizes then concatenate all.", # Description
        "MH_DSCNet", # Name
        ['KERNEL', 'F1', 'PD', 'DR', 'EP', 'BS', 'PO', 'AC'], 
            # Parameters [
            #   KERNEL_SIZE, 
            #   FILTERS, 
            #   PADDINGMODE,
            #   DROP_OUT, 
            #   EPOCHS, 
            #   BATCHSIZE, 
            #   PARTIALOUTPUTCLASSES, 
            #   ACTIVATION]
        'JSModel_MH_DSCNet' # model
        )

    print("EXPERIMENT NAME: ", jsExperiment.name)
    
    jsLog = JSLog(jsExperiment.name)

    data_type = 'raw'
    jsData = JSData(data_type)

    num_subjects = 60
    index_subjects = np.arange(1,num_subjects+1)

    kf = KFold(n_splits=10)

    jsExperiment.start_experiment()

    jsLog.init_results(jsExperiment.parameters)
    fold_iter = 1
    for train, test in kf.split(index_subjects):
        
        train = train + 1
        test = test +1
        print("%s %s" % (train, test))

        partial_x_train, partial_y_train, x_val, y_val, x_test, y_test = jsData.JSGetTrainTest_Categorical( train, test, 5, int_seed)

        print(partial_x_train.shape)
        print(partial_y_train.shape)

        print(x_val.shape)
        print(y_val.shape)

        print(x_test.shape)
        print(y_test.shape)

        print(partial_x_train[0])

        print(partial_x_train[0])

        print("Shape dims: ")
        print(partial_x_train.shape)

        for param_filters in [8, 16, 32, 64]:
            for param_activation in ['elu', 'relu']:
                for param_partial_output_classes in [9, partial_x_train.shape[1], partial_x_train.shape[1]*2, partial_x_train.shape[1]*4]:
                    for param_padding_mode in ['valid', 'same']:
                        for param_epochs in [15, 30, 60]:
        
                            param_kernel_size = 128
                            param_dropout = 0.5
                            param_batch_size = 128

                            experiment_parameters_values = [
                                param_kernel_size, 
                                param_filters,
                                param_padding_mode, 
                                param_dropout, 
                                param_epochs, 
                                param_batch_size,
                                param_partial_output_classes,
                                param_activation]

                            model = JSModel_MH_DSCNet(
                                Chans = partial_x_train.shape[1], 
                                Samples = partial_x_train.shape[2], 
                                nb_classes=2, 
                                dropout_rate=param_dropout, 
                                filters = param_filters,
                                kernel_size=param_kernel_size, 
                                default_activation=param_activation,
                                partial_output_classes=param_partial_output_classes,
                                conv_1D_padding=param_padding_mode)


                            model.compile(
                                optimizer='adam',
                                loss='binary_crossentropy',
                                metrics=[tf.keras.metrics.BinaryAccuracy(),
                                        tf.keras.metrics.Precision(),
                                        tf.keras.metrics.Recall(),
                                        tf.keras.metrics.TruePositives(),
                                        tf.keras.metrics.TrueNegatives(),
                                        tf.keras.metrics.FalsePositives(),
                                        tf.keras.metrics.FalseNegatives()])

                            print(model.summary())
                            print(experiment_parameters_values)

                            history = model.fit(
                                partial_x_train,
                                partial_y_train,
                                epochs=param_epochs,
                                batch_size=param_batch_size,
                                validation_data=(x_val, y_val),
                                verbose=1)

                            results = model.evaluate(x_test, y_test)

                            jsLog.JSSaveResults(
                                experiment_parameters_values, 
                                fold_iter, 
                                results)
                    
                            jsLog.JSSaveHistory(
                                history, 
                                experiment_parameters_values, 
                                jsExperiment.parameters, 
                                jsExperiment.get_history_filename(
                                    experiment_parameters_values,
                                    fold_iter))
                    
                            jsLog.JSEvaluateBySubjectBinary(
                                model,
                                jsExperiment.parameters,
                                experiment_parameters_values,
                                test, 
                                jsData, 
                                jsExperiment.get_predictions_filename(
                                    experiment_parameters_values,
                                    fold_iter))

        fold_iter = fold_iter + 1