# -*- coding: utf-8 -*-

import random
import os


import numpy as np
import tensorflow as tf

from sklearn.model_selection import KFold

from JSLog import JSLog
from JSData import JSData
from JSModels import JSModel_MLP_03
from JSExperiment import JSExperiment


def JSExperiment_MLP_03():
    int_seed = 1234
    str_seed = '1234'

    os.environ['PYTHONHASHSEED'] = str_seed
    os.environ['TF_DETERMINISTIC_OPS'] = '1'

    np.random.seed(int_seed)
    random.seed(int_seed)
    tf.random.set_seed(int_seed)

    jsExperiment = JSExperiment(
        "MultiLayer Perceptron with automatic clean data with ASR.", # Description
        "MLP_03_C", # Name
        ['U1', 'U2', 'U3', 'DR', 'BS', 'AC', 'BI', 'EP'], 
            # Parameters [
            #   UNITS Layer 1, 
            #   UNITS Layer 2, 
            #   UNITS Layer 3, 
            #   DROPOUT, 
            #   BATCHSIZE, 
            #   ACTIVATION,
            #   BIAS,
            #   EPOCHS]
        'JSModel_MLP_03' # model
        )

    print("EXPERIMENT NAME: ", jsExperiment.name)

    jsLog = JSLog(jsExperiment.name)

    data_type = 'raw'
    jsData = JSData(data_type)

    num_subjects = 60
    index_subjects = np.arange(1,num_subjects+1)

    kf = KFold(n_splits=10)

    jsExperiment.start_experiment()

    jsLog.init_results(jsExperiment.parameters)
    fold_iter = 1
    for train, test in kf.split(index_subjects):
        
        train = train + 1
        test = test +1
        print("%s %s" % (train, test))

        partial_x_train, partial_y_train, x_val, y_val, x_test, y_test = jsData.JSGetTrainTest_Categorical( train, test, 5, int_seed)

        print(partial_x_train.shape)
        print(partial_y_train.shape)

        print(x_val.shape)
        print(y_val.shape)

        print(x_test.shape)
        print(y_test.shape)

        for paramU1 in [32, 64, 128, 256]:
            for paramU2 in [32, 64, 128, 256]:
                for paramU3 in [32, 64, 128, 256]:
                    for param_activation in ['relu']:
                        for param_bias in [True, False]:
                            for param_epochs in [15, 30, 60, 120]:

                                param_dropout = 0.5
                                param_batch_size = 128
                                
                                # param_epochs = 15

                                experiment_parameters_values = [
                                    paramU1, 
                                    paramU2, 
                                    paramU3, 
                                    param_dropout, 
                                    param_batch_size,
                                    param_activation,
                                    param_bias,
                                    param_epochs]

                                model = JSModel_MLP_03(
                                    chans=partial_x_train.shape[1], 
                                    samples=partial_x_train.shape[2],  
                                    U1 = paramU1, 
                                    U2 = paramU2, 
                                    U3 = paramU3, 
                                    use_bias_in = param_bias, 
                                    dropout_block = param_dropout, 
                                    nb_classes=2, 
                                    dense_activation=param_activation)

                                model.compile(
                                    optimizer='adam',
                                    loss='binary_crossentropy',
                                                                   
                                    metrics=[tf.keras.metrics.BinaryAccuracy(),
                                            tf.keras.metrics.Precision(),
                                            tf.keras.metrics.Recall(),
                                            tf.keras.metrics.TruePositives(),
                                            tf.keras.metrics.TrueNegatives(),
                                            tf.keras.metrics.FalsePositives(),
                                            tf.keras.metrics.FalseNegatives()])

                                print(model.summary())
                                print(experiment_parameters_values)

                                history = model.fit(
                                    partial_x_train,
                                    partial_y_train,
                                    epochs=param_epochs,
                                    batch_size=param_batch_size,
                                    validation_data=(x_val, y_val),
                                    verbose=1)

                                results = model.evaluate(x_test, y_test)
                                print(results)
                                

                                jsLog.JSSaveResults(
                                    experiment_parameters_values, 
                                    fold_iter, 
                                    results)
                
                                jsLog.JSSaveHistory(
                                    history, 
                                    experiment_parameters_values, 
                                    jsExperiment.parameters, 
                                    jsExperiment.get_history_filename(
                                        experiment_parameters_values,
                                        fold_iter))
                
                                jsLog.JSEvaluateBySubjectBinary(
                                    model,
                                    jsExperiment.parameters,
                                    experiment_parameters_values,
                                    test, 
                                    jsData, 
                                    jsExperiment.get_predictions_filename(
                                        experiment_parameters_values,
                                        fold_iter))

        fold_iter = fold_iter + 1