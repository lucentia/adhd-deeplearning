# -*- coding: utf-8 -*-

import random
import os
from tabnanny import verbose


import numpy as np
import tensorflow as tf

from sklearn.model_selection import KFold

from JSLog import JSLog
from JSData import JSData
from JSModels import JSModel_MH_EEGNet
from JSExperiment import JSExperiment



def JSExperiment_MH_EEGNet():
    int_seed = 1234
    str_seed = '1234'

    os.environ['PYTHONHASHSEED'] = str_seed
    os.environ['TF_DETERMINISTIC_OPS'] = '0'

    np.random.seed(int_seed)
    random.seed(int_seed)
    tf.random.set_seed(int_seed)

    jsExperiment = JSExperiment(
        "EEGNetMultiHead with automatic clean data with ASR", # Description
        "MH_EEGNet", # Name
        ['SR', 'F1', 'F2', 'D', 'DR', 'DT', 'EP','BS'], # Parameters
        'JSModel_MH_EEGNet' # model
        )

    print("EXPERIMENT NAME: ", jsExperiment.name)
    
    jsLog = JSLog(jsExperiment.name)

    data_type = 'raw'
    jsData = JSData(data_type)

    num_subjects = 60
    index_subjects = np.arange(1,num_subjects+1)

    kf = KFold(n_splits=10)

    jsExperiment.start_experiment()

    jsLog.init_results(jsExperiment.parameters)
    fold_iter = 1
    for train, test in kf.split(index_subjects):
        
        train = train + 1
        test = test +1
        print("%s %s" % (train, test))

        partial_x_train, partial_y_train, x_val, y_val, x_test, y_test = jsData.JSGetTrainTest_Categorical( train, test, 5, int_seed)

        print(partial_x_train.shape)
        print(partial_y_train.shape)

        print(x_val.shape)
        print(y_val.shape)

        print(x_test.shape)
        print(y_test.shape)

        print(partial_x_train[0])

        # Shape adaptation for EEGNet
        partial_x_train = np.expand_dims(partial_x_train,3)
        x_val = np.expand_dims(x_val,3)
        x_test = np.expand_dims(x_test,3)

        print(partial_x_train[0])

        print("Shape dims: ")
        print(partial_x_train.shape)
        
        for param_experiment_epochs in [15, 30, 60]:
            for param_F1 in [256, 128, 64, 32]:
                for param_F2 in [128, 64, 32, 16]:
                    param_SamplingRate = 128
                    
                    
                                
                    param_D = 2 
                    param_dropout_rate = 0.5
                    
                    param_experiment_batch_size = 256

                    experiment_parameters_values = [
                        param_SamplingRate, 
                        param_F1, 
                        param_F2, 
                        param_D, 
                        param_dropout_rate, 
                        data_type, 
                        param_experiment_epochs, 
                        param_experiment_batch_size]

                    model = JSModel_MH_EEGNet(
                        1, 
                        Chans = partial_x_train.shape[1], 
                        Samples = partial_x_train.shape[2], 
                        dropoutRate = param_dropout_rate, 
                        samplingRate = param_SamplingRate, 
                        F1 = param_F1, 
                        D = param_D, 
                        F2 = param_F2, 
                        norm_rate = 0.25, 
                        dropoutType = 'Dropout', 
                        activation='elu')

                    model.compile(
                        optimizer='adam',
                        loss='binary_crossentropy',
                        metrics=[tf.keras.metrics.BinaryAccuracy(),
                                tf.keras.metrics.Precision(),
                                tf.keras.metrics.Recall(),
                                tf.keras.metrics.TruePositives(),
                                tf.keras.metrics.TrueNegatives(),
                                tf.keras.metrics.FalsePositives(),
                                tf.keras.metrics.FalseNegatives()])

                    
                    print(model.summary())
                    print(experiment_parameters_values)

                    history = model.fit(
                        partial_x_train,
                        partial_y_train,
                        epochs=param_experiment_epochs,
                        batch_size=param_experiment_batch_size,
                        validation_data=(x_val, y_val),
                        verbose=1)

                    results = model.evaluate(x_test, y_test)

                    jsLog.JSSaveResults(
                        experiment_parameters_values, 
                        fold_iter, 
                        results)

                    jsLog.JSSaveHistory(
                        history, 
                        experiment_parameters_values, 
                        jsExperiment.parameters, 
                        jsExperiment.get_history_filename(
                            experiment_parameters_values,
                            fold_iter))

                    jsLog.JSEvaluateBySubjectBinary(
                        model,
                        jsExperiment.parameters,
                        experiment_parameters_values,
                        test, 
                        jsData, 
                        jsExperiment.get_predictions_filename(
                            experiment_parameters_values,
                            fold_iter))

        fold_iter = fold_iter + 1