import sys
import json

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

import seaborn as sns
import pingouin as pg

from scipy.stats import ttest_ind
from scipy.stats import ttest_rel
from scipy.stats import f_oneway
from scipy.stats import t



from os import path

def createBoxBlot(ds, parameter, title, ylabel, figure_filename):
    fig = plt.figure(figsize=(6.000, 3.708))
    sns.boxplot(x='f1-score', y=parameter, data=ds, orient='h')
    sns.swarmplot(x='f1-score', y=parameter, data=ds, color='black', alpha = 0.5, orient='h', size=2)
    plt.title(title, weight='bold')
    plt.xlabel('f1-score', fontsize=13)
    plt.ylabel(ylabel, fontsize=13)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12, weight='bold')
    #plt.margins(0.5)
    plt.subplots_adjust(bottom=0.15)
    plt.savefig(figure_filename, dpi=300)

def createFigure(location_data, title, variable_name, figure_file_name):

    plt.style.use('seaborn-whitegrid')
    fig, ax = plt.subplots(figsize=(6.000, 3.708))

    if (len(location_data) == 2):
        index = 1
        index_increment = 1
    elif (len(location_data) == 3):
        index = 0.5
        index_increment = 1
    elif (len(location_data) == 4):
        index = 0
        index_increment = 1
    else:
        index = 0
        index_increment = 1

    colors = ['#3274a1', '#e1812c','#3a923a', '#c03d3e']
    y_ticks_labels = ['', '', '', '', '', '', '', '']
    y_ticks_index = {0:1, 0.5:2, 1:3, 1.5:4, 2:5, 2.5:6, 3:7}
    index_item = len(location_data) - 1
    for key, values in location_data.items():
        print(key)
        print(values)
        
        ax.errorbar(values["mean"], index, xerr=(values["conf_interval"][1] - values["conf_interval"][0]) / 2,
                    fmt='o', markersize=8, elinewidth=3, capsize=5, capthick=3, label=key, color=colors[index_item])
        y_ticks_labels[y_ticks_index[index]] = key
        index += index_increment
        index_item -= 1

    ##ax.errorbar(lh_data["mean"], 1, xerr=(conf_lh[1] - conf_lh[0]) / 2,
    #            fmt='o', markersize=8, capsize=5, label='LH', color='grey')

    #ax.set_ylim(-0.6, 3.6)
    #ax.fill_betweenx([1, 0], conf_lh[0], conf_rh[1], facecolor='lightgrey', alpha=0.3)
    #ax.legend(loc='best', fontsize=11, framealpha=1, frameon=True)
    ax.set_xlabel('f1-score')
    ax.set_ylabel(variable_name)
    ax.yaxis.set_major_formatter(plt.NullFormatter())
    
    plt.xlabel('f1-score', fontsize=13)
    plt.ylabel(variable_name, fontsize=13)
    plt.xticks(fontsize=12)
    plt.yticks(np.arange(-0.5, 3.5, 0.5), y_ticks_labels, fontsize=12, weight='bold')
    plt.xlim([0.71, 0.77])
    plt.ylim([-0.5, 3.5])
    plt.subplots_adjust(bottom=0.15)
    plt.title(title, weight='bold')
    plt.savefig(figure_file_name, dpi=300)

def getLocationStatistics(feature, value, data, y_value, alpha):
    
    location_data = {
        "count": data.loc[(data[feature] == value)][y_value].count(),
        "mean": data.loc[(data[feature] == value)][y_value].mean(),
        "std": data.loc[(data[feature] == value)][y_value].std()
    }

    location_data["conf_interval"] = t.interval(
        1 - alpha, 
        location_data["count"] - 1, 
        loc=location_data["mean"], 
        scale= location_data["std"] / np.sqrt(location_data["count"]))

    return location_data


log_path = path.dirname(path.realpath(__file__))
print(log_path)


alpha = 0.05
y_value = 'f1-score'

data_path = path.join(log_path,'HyperparameterTuningDataSet.csv')
figure_output_path = path.join(log_path,'plots')
dataset = pd.read_csv(data_path, delimiter=';')


print(dataset.describe())

##
## NUMBER OF FILTERS
##

print("FILTERS: ")
filter_ds = dataset[['NF', y_value]]
print(filter_ds.groupby('NF').describe())

print(f_oneway(
    filter_ds[filter_ds['NF']==8][y_value],
    filter_ds[filter_ds['NF']==16][y_value],
    filter_ds[filter_ds['NF']==32][y_value],
    filter_ds[filter_ds['NF']==64][y_value]))

loc_stats = {}

loc_stats['64'] = getLocationStatistics('NF', 64, filter_ds, y_value, alpha)
loc_stats['32'] = getLocationStatistics('NF', 32, filter_ds, y_value, alpha)
loc_stats['16'] = getLocationStatistics('NF', 16, filter_ds, y_value, alpha)
loc_stats['8'] = getLocationStatistics('NF', 8, filter_ds, y_value, alpha)

createFigure(loc_stats, 'Number of Filters Confidence Interval', 'Number of Filters', path.join(figure_output_path,'filters.png'))
createBoxBlot(filter_ds, 'NF', 'Number of Filters Experiment - BoxPlot', 'Number of Filters', path.join(figure_output_path,'filters_boxplot.png'))


##
## ACTIVATION
##

print("ACTIVATION: ")

activation_ds = dataset[['AF', y_value]]
print(activation_ds.groupby('AF').describe())

cat1 = activation_ds[activation_ds['AF']=='elu']
cat2 = activation_ds[activation_ds['AF']=='relu']

print(ttest_ind(cat1[y_value], cat2[y_value]))

loc_stats = {}
loc_stats['relu'] = getLocationStatistics('AF', 'relu', activation_ds, y_value, alpha)
loc_stats['elu'] = getLocationStatistics('AF', 'elu', activation_ds, y_value, alpha)


print(loc_stats)

createFigure(loc_stats, 'Activation Function Confidence Interval', 'Activation Function', path.join(figure_output_path,'activation.png'))
createBoxBlot(activation_ds, 'AF', 'Activation Function Experiment - BoxPlot', 'Activation Function', path.join(figure_output_path,'activation_boxplot.png'))

##
## PADDING
##

print("PADDING: ")

padding_ds = dataset[['PM', y_value]]
print(padding_ds.groupby(['PM']).describe())

cat1 = padding_ds[padding_ds['PM']=='same']
cat2 = padding_ds[padding_ds['PM']=='valid']

print(ttest_ind(cat1[y_value], cat2[y_value]))

loc_stats = {}
loc_stats['same'] = getLocationStatistics('PM', 'same', padding_ds, y_value, alpha)
loc_stats['valid'] = getLocationStatistics('PM', 'valid', padding_ds, y_value, alpha)

print(loc_stats)

createFigure(loc_stats, 'Padding Mode Confidence Interval', 'Padding Mode', path.join(figure_output_path,'padding.png'))
createBoxBlot(padding_ds, 'PM', 'Padding Mode Experiment - BoxPlot', 'Padding Mode', path.join(figure_output_path,'padding_boxplot.png'))

##
## NUMBER UNITS
##

print("NUMBER UNITS: ")
numberunits_ds = dataset[['NU', y_value]]
print(numberunits_ds.groupby('NU').describe())

print(f_oneway(
    numberunits_ds[numberunits_ds['NU']==9][y_value],
    numberunits_ds[numberunits_ds['NU']==19][y_value],
    numberunits_ds[numberunits_ds['NU']==38][y_value],
    numberunits_ds[numberunits_ds['NU']==76][y_value]))

loc_stats = {}



loc_stats['76'] = getLocationStatistics('NU', 76, numberunits_ds, y_value, alpha)
loc_stats['38'] = getLocationStatistics('NU', 38, numberunits_ds, y_value, alpha)
loc_stats['19'] = getLocationStatistics('NU', 19, numberunits_ds, y_value, alpha)
loc_stats['9']  = getLocationStatistics('NU',  9, numberunits_ds, y_value, alpha)

createFigure(loc_stats, 'Number of Units Confidence Interval', 'Number of Units', path.join(figure_output_path,'numberunits.png'))
createBoxBlot(numberunits_ds, 'NU', 'Number of Units Experiment - BoxPlot', 'Number of Units', path.join(figure_output_path,'numberunits_boxplot.png'))

##
## EPOCHS
##

print("EPOCHS: ")
epochs_ds = dataset[['EP', y_value]]
print(epochs_ds.groupby('EP').describe())

print(f_oneway(
    epochs_ds[epochs_ds['EP']==15][y_value],
    epochs_ds[epochs_ds['EP']==30][y_value],
    epochs_ds[epochs_ds['EP']==60][y_value]))

loc_stats = {}
loc_stats['60'] = getLocationStatistics('EP', 60, epochs_ds, y_value, alpha)
loc_stats['30'] = getLocationStatistics('EP', 30, epochs_ds, y_value, alpha)
loc_stats['15'] = getLocationStatistics('EP', 15, epochs_ds, y_value, alpha)

createFigure(loc_stats, 'Number of Epochs Confidence Interval', 'Number of Epochs', path.join(figure_output_path,'epochs.png'))
createBoxBlot(epochs_ds, 'EP', 'Number of Epochs Experiment - BoxPlot', 'Number of Epochs', path.join(figure_output_path,'epochs_boxplot.png'))

sys.exit()