# -*- coding: utf-8 -*-

import random
import os
from tabnanny import verbose


import numpy as np
import tensorflow as tf

from sklearn.model_selection import KFold

from JSLog import JSLog
from JSData import JSData
from JSModels import JSModel_VGGNet
from JSExperiment import JSExperiment

def JSExperiment_VGGNet():
    int_seed = 1234
    str_seed = '1234'

    os.environ['PYTHONHASHSEED'] = str_seed
    os.environ['TF_DETERMINISTIC_OPS'] = '1'

    np.random.seed(int_seed)
    random.seed(int_seed)
    tf.random.set_seed(int_seed)

    jsExperiment = JSExperiment(
        "VGGNet with automatic clean data with ASR", # Description
        "VGGNet_RAW", # Name
        ['BS'], # Parameters
        'VGGNet' # model
        )

    print("EXPERIMENT NAME: ", jsExperiment.name)
    
    jsLog = JSLog(jsExperiment.name)

    data_type = 'raw'
    jsData = JSData(data_type)

    num_subjects = 60
    index_subjects = np.arange(1,num_subjects+1)

    kf = KFold(n_splits=10)

    jsExperiment.start_experiment()

    jsLog.init_results(jsExperiment.parameters)
    fold_iter = 1
    for train, test in kf.split(index_subjects):
        
        train = train + 1
        test = test +1
        print("%s %s" % (train, test))

        partial_x_train, partial_y_train, x_val, y_val, x_test, y_test = jsData.JSGetTrainTest_Categorical( train, test, 5, int_seed)

        print(partial_x_train.shape)
        print(partial_y_train.shape)

        print(x_val.shape)
        print(y_val.shape)

        print(x_test.shape)
        print(y_test.shape)

        

        # Shape adaptation for VGGNet
        partial_x_train = np.expand_dims(partial_x_train,3)
        x_val = np.expand_dims(x_val,3)
        x_test = np.expand_dims(x_test,3)

        

        print("Shape dims: ")
        print(partial_x_train.shape)
        
        for experiment_epochs in [15, 30, 60, 90, 120, 400]:
    
            experiment_parameters_values = [experiment_epochs]
            experiment_batch_size = 128

            model = JSModel_VGGNet(
                nb_classes = 2, 
                height = partial_x_train.shape[1], 
                width = partial_x_train.shape[2],
                depth = partial_x_train.shape[3])

            model.compile(
                optimizer='adam',
                loss='binary_crossentropy',
                metrics=[tf.keras.metrics.BinaryAccuracy(),
                        tf.keras.metrics.Precision(),
                        tf.keras.metrics.Recall(),
                        tf.keras.metrics.TruePositives(),
                        tf.keras.metrics.TrueNegatives(),
                        tf.keras.metrics.FalsePositives(),
                        tf.keras.metrics.FalseNegatives()])

            print(model.summary())
            print(experiment_parameters_values)
            
            history = model.fit(
                partial_x_train,
                partial_y_train,
                epochs=experiment_epochs,
                batch_size=experiment_batch_size,
                validation_data=(x_val, y_val),
                verbose=1)
            
            results = model.evaluate(x_test, y_test)

            jsLog.JSSaveResults(
                experiment_parameters_values, 
                fold_iter, 
                results)

            jsLog.JSSaveHistory(
                history, 
                experiment_parameters_values, 
                jsExperiment.parameters, 
                jsExperiment.get_history_filename(
                    experiment_parameters_values,
                    fold_iter))
    
            jsLog.JSEvaluateBySubjectBinary(
                model,
                jsExperiment.parameters,
                experiment_parameters_values,
                test, 
                jsData, 
                jsExperiment.get_predictions_filename(
                    experiment_parameters_values,
                    fold_iter))

        fold_iter = fold_iter + 1